//#include <stdio.h>
//#include <stdlib.h>
//
//typedef enum { TRUE = 0, FALSE = -1} boolean;
//
//struct Array {
//	int* A;	// An array of ints (size defined at initialisation)
//	int size;	// Max number of elements array can contain
//	int length;	// Number of populated elements in array
//};
//
//void Swap(int* x, int* y) {
//
//	int temp = *x;
//	*x = *y;
//	*y = temp;
//}
//
//boolean IsSorted(struct Array arr) {
//	for (int i = 0; i < arr.length - 1; i++) {
//		if (arr.A[i] > arr.A[i + 1]){
//			return FALSE;
//		}
//	}
//	return TRUE;
//}
//
//// Note array is call-by-value because array is not being modified
//void Display(struct Array arr) {
//	for (int i = 0; i < arr.length; i++) {
//		printf("%d ", arr.A[i]);
//	}
//	printf("\n\n");
//}
//
//// Note array is call-by-reference because array is being modified
//void Append(struct Array *arr, int elem) {
//	if (arr->size <= arr->length) {
//		printf("Error: Function Append() - Can't append any more elements. Array is full!\n\n");
//	}
//	else {
//		printf("Appending value %d at index %d\n\n", elem, arr->length);
//		arr->A[arr->length] = elem;
//		arr->length++;
//	}
//}
//
//void Insert(struct Array* arr, int index, int elem) {
//	if (arr->size <= arr->length) {
//		printf("Error: Function Insert() - Can't insert any more elements. Array is full!\n\n");
//	}
//	else if (index > arr->length) {
//		printf("Error: Function Insert() - Can't insert element beyond current last element (length is %d, requested index is %d)\n\n", arr->length, index);
//	}
//	else if (index < 0) {
//		printf("Error: Function Insert() - Can't insert element at negative index %d\n\n", index);
//	}
//	else {
//		printf("Inserting value %d at index %d\n\n", elem, index);
//		for (int i = arr->length; i > index; i--) {
//			arr->A[i] = arr->A[i - 1];
//		}
//		arr->A[index] = elem;
//		arr->length++;
//	}
//}
//
//void Delete(struct Array* arr, int index) {
//	if (index >= arr->length || index < 0) {
//		printf("Error - Function Delete() - No element at index %d, so can't delete!\n\n", index);
//	}
//	else {
//		printf("Deleting value %d at index %d\n\n", arr->A[index], index);
//		for (int i = index; i < arr->length - 1; i++) {
//			arr->A[i] = arr->A[i + 1];
//		}
//		//arr->A[arr->length - 1] = NULL;
//		arr->length--;
//	}
//}
//
//int LinearSearch(struct Array* arr, int key) {
//	for (int i = 0; i < arr->length; i++) {
//		if (arr->A[i] == key) {
//			// Optimisation method #1: move element back one place
//			//if (i > 0) {
//			//	swap(&arr->A[i], &arr->A[i - 1]);
//			//}
//
//			// Optimisation method #2: move element to head
//			// if (i > 0) {
//			//	Swap(&arr->A[0], &arr->A[i]);
//			// }
//			return i;
//		}
//	}
//
//	return -1; // Default, no match
//}
//
//// Binary search, use for sorted arrays
//int BinarySearch(struct Array arr, int key) {
//	int low = 0;
//	int high = arr.length - 1;
//	int mid = 0;
//
//	while (low <= high) {
//		mid = (low + high) / 2;
//		if (key == arr.A[mid]) { // Match
//			return mid;
//		}
//		else if (key < arr.A[mid]) { // Key is in lower half of low..high
//			high = mid - 1;
//		}
//		else { // Key is in upper half of low..high
//			low = mid + 1;
//		}
//	}
//	return -1; // No match
//}
//
//// Recursive version of loop-based binary search
//// Note using a[] instead of the arr struct defined in main? I think the instructor did this
//// as it makes the function more generic.
//int RBinarySearch(int a[], int low, int high, int key) {
//	if (low <= high) {
//		int mid = (low + high) / 2;
//		if (key == a[mid]) { // Match
//			return mid;
//		}
//		else if (key < a[mid]) { // Key is in lower half of low..high
//			return RBinarySearch(a, low, mid - 1, key);
//		}
//		else { // Key is in upper half of low..high
//			return RBinarySearch(a, mid + 1, high, key);
//		}
//	}
//	return -1; // No match
//}
//
//int Get(struct Array arr, int index) {
//	int index_value = -1;
//	if (index >= arr.length) {
//		printf("Error: Function Get() - Index %d is greater than length of array (%d).\n\n", index, arr.length);
//	}
//	else if (index < 0) {
//		printf("Error: Function Get() - Index %d is less than zero.\n\n", index);
//	}
//	else {
//		index_value = arr.A[index];
//	}
//	return index_value;
//}
//
//void Set(struct Array *arr, int index, int value) {
//	if (index >= arr->length) {
//		printf("Error: Function Set() - Index %d is greater than length of array (%d).\n\n", index, arr->length);
//	}
//	else if (index < 0) {
//		printf("Error: Function Set() - Index %d is less than zero.\n\n", index);
//	}
//	else {
//		printf("Setting value at index %d to %d\n\n", index, value);
//		arr->A[index] = value;
//	}
//}
//
//// Use for unsorted list, checks all elements.
//// Time: 2n + 1 = O(n), could be very time costly!!!
//int Max(struct Array arr) {
//	int max = arr.A[0];
//	for (int i = 1; i < arr.length; i++) {
//		if (arr.A[i] > max) {
//			max = arr.A[i];
//		}
//	}
//	return max;
//}
//
//// Use for unsorted list, checks all elements.
//// Time: 2n + 1 = O(n), could be very time costly!!!
//int Min(struct Array arr) {
//	int min = arr.A[0];
//	for (int i = 1; i < arr.length; i++) {
//		if (arr.A[i] < min) {
//			min = arr.A[i];
//		}
//	}
//	return min;
//}
//
//// Time: 2n + 3 = O(n)
//int Sum(struct Array arr) {
//	int sum = 0;
//	for (int i = 0; i < arr.length; i++) {
//		sum += arr.A[i];
//	}
//	return sum;
//}
//
//// Recursive sum
//int RSum(struct Array arr, int n) {
//	if (n < 0) {
//		return 0;
//	}
//	return RSum(arr, n - 1) + arr.A[n];
//}
//
//// Time: 2n + 3 = O(n)
//float Average(struct Array arr) {
//	// Note that we already have a sum function, so why repeat the code?!
//	return (float)Sum(arr) / arr.length;
//}
//
//void Reverse(struct Array* arr) {
//	int* tmp_arr = (int *)malloc(arr->length * sizeof(int));
//
//	for (int i = arr->length - 1, j = 0; i >= 0; i--, j++) {
//		tmp_arr[j] = arr->A[i];
//	}
//	for (int i = 0; i < arr->length; i++) {
//		arr->A[i] = tmp_arr[i];
//	}
//}
//
//void SwapReverse(struct Array* arr) {
//	for (int i = 0, j = arr->length - 1; i < j; i++, j--) {
//		Swap(&arr->A[i], &arr->A[j]);
//	}
//}
//
//void LeftShift(struct Array* arr) {
//	for (int i = 1; i < arr->length; i++) {
//		arr->A[i - 1] = arr->A[i];
//	}
//	arr->A[arr->length - 1] = 0;
//	arr->length--;
//}
//
//void LeftRotate(struct Array* arr) {
//	int tmp = arr->A[0];
//	LeftShift(arr);
//	arr->length++;
//	arr->A[arr->length - 1] = tmp;
//}
//
//void RightShift(struct Array* arr) {
//	for (int i = arr->length - 1; i > 0 ; i--) {
//		arr->A[i] = arr->A[i - 1];
//	}
//	arr->A[0] = 0;
//	arr->length--;
//}
//
//void RightRotate(struct Array* arr) {
//	int tmp = arr->A[arr->length - 1];
//	RightShift(arr);
//	arr->length++;
//	arr->A[0] = tmp;
//}
//
//void InsertSort(struct Array* arr, int elem) {
//	int i = arr->length - 1;
//
//	if (arr->length == arr->size) { // No free space in array so can't insert
//		return;
//	}
//
//	while (i >= 0 && arr->A[i] > elem) {
//		arr->A[i + 1] = arr->A[i];
//		i--;
//	}
//
//	arr->A[i + 1] = elem;
//	arr->length++;
//}
//
//void Rearrange(struct Array* arr) {
//	int i = 0;
//	int j = arr->length - 1;
//
//	while (i < j) {
//		while (arr->A[i] < 0) {
//			i++;
//		}
//		while (arr->A[j] >= 0) {
//			j--;
//		}
//		if (i < j) {
//			Swap(&arr->A[i], &arr->A[j]);
//		}
//	}
//}
//
//struct Array* MergeArrays(struct Array arr1, struct Array arr2) {
//	int i = 0; // arr1 index
//	int j = 0; // arr2 index
//	int k = 0; // merged array index
//	struct Array* arr3 = (struct Array*)malloc(sizeof(struct Array)); // Create a new array in heap for the merged data
//	arr3->size = arr1.size + arr2.size;
//	arr3->length = arr1.length + arr2.length;
//	arr3->A = (int*)malloc(arr3->size * sizeof(int));
//
//	while (i < arr1.length && j < arr2.length) {
//		if (arr1.A[i] <= arr2.A[j]) {
//			arr3->A[k++] = arr1.A[i++];
//		}
//		else {
//			arr3->A[k++] = arr2.A[j++];
//		}
//	}
//
//	for (; i < arr1.length; i++) {
//		arr3->A[k++] = arr1.A[i++];
//	}
//	for (; j < arr2.length; j++) {
//		arr3->A[k++] = arr2.A[j++];
//	}
//
//	return arr3;
//}
//
//// Time take: Omega(m + (m * n)), or Omega(n^2)
//struct Array* Union(struct Array arr1, struct Array arr2) {
//	struct Array* arr3 = (struct Array*)malloc(sizeof(struct Array)); // Create a new array in heap for the merged data
//	arr3->size = arr1.size + arr2.size;
//	arr3->length = 0;
//	arr3->A = (int*)malloc(arr3->size * sizeof(int));
//
//	for (int a1_idx = 0; a1_idx < arr1.length; a1_idx++) {
//		arr3->A[arr3->length++] = arr1.A[a1_idx];
//	}
//
//	for (int a2_idx = 0; a2_idx < arr2.length; a2_idx++) {
//		if (LinearSearch(arr3, arr2.A[a2_idx]) == -1) {
//			arr3->A[arr3->length++] = arr2.A[a2_idx];
//		}
//	}
//
//	return arr3;
//}
//
//// Time take: Omega(m + n), or Omega(n)
//struct Array* UnionSorted(struct Array arr1, struct Array arr2) {
//	int arr1_idx = 0;
//	int arr2_idx = 0;
//
//	struct Array* arr3 = (struct Array*)malloc(sizeof(struct Array)); // Create a new array in heap for the merged data
//	arr3->size = arr1.size + arr2.size;
//	arr3->A = (int*)malloc(arr3->size * sizeof(int));
//	arr3->length = 0;
//
//	while (arr1_idx < arr1.length && arr2_idx < arr2.length) {
//		if (arr1.A[arr1_idx] < arr2.A[arr2_idx]) { // Put elem from array 1 into array 3
//			arr3->A[arr3->length++] = arr1.A[arr1_idx++];
//		}
//		else if (arr2.A[arr2_idx] < arr1.A[arr1_idx]) { // Put elem from array 2 into array 3
//			arr3->A[arr3->length++] = arr2.A[arr2_idx++];
//		}
//		else {
//			arr3->A[arr3->length++] = arr1.A[arr1_idx++];
//			arr2_idx++; // Increment both arr1 and arr2 indexes as values are the same
//		}
//	}
//
//	for (; arr1_idx < arr1.length; arr1_idx++) { // Vacuum up any dregs of array 1 and dump into array 3
//		arr3->A[arr3->length++] = arr1.A[arr1_idx++];
//	}
//
//	for (; arr2_idx < arr2.length; arr2_idx++) { // Vacuum up any dregs of array 2 and dump into array 3
//		arr3->A[arr3->length++] = arr2.A[arr2_idx++];
//	}
//
//	return arr3;
//}
//
//// Time take: Omega(m * n), or Omega(n^2)
//struct Array* Intersection(struct Array arr1, struct Array arr2) {
//	int arr1_idx = 0;
//	int arr2_idx = 0;
//
//	struct Array* arr3 = (struct Array*)malloc(sizeof(struct Array)); // Create a new array in heap for the merged data
//	arr3->size = arr1.size + arr2.size;
//	arr3->length = 0;
//	arr3->A = (int*)malloc(arr3->size * sizeof(int));
//
//	while (arr2_idx < arr2.length) {
//		if (LinearSearch(&arr1, arr2.A[arr2_idx]) != -1) { // If match found
//			arr3->A[arr3->length++] = arr2.A[arr2_idx];
//		}
//		arr2_idx++;
//	}
//
//	return arr3;
//}
//
//struct Array* IntersectionSorted(struct Array arr1, struct Array arr2) {
//	int arr1_idx = 0;
//	int arr2_idx = 0;
//
//	struct Array* arr3 = (struct Array*)malloc(sizeof(struct Array)); // Create a new array in heap for the merged data
//	arr3->size = arr1.size + arr2.size;
//	arr3->length = 0;
//	arr3->A = (int*)malloc(arr3->size * sizeof(int));
//
//	while (arr1_idx < arr1.length && arr2_idx < arr2.length) {
//		if (arr1.A[arr1_idx] < arr2.A[arr2_idx]) {
//			arr1_idx++;
//		}
//		else if (arr1.A[arr1_idx] > arr2.A[arr2_idx]) {
//			arr2_idx++;
//		}
//		else {
//			arr3->A[arr3->length++] = arr1.A[arr1_idx];
//			arr1_idx++;
//			arr2_idx++;
//		}
//	}
//
//	return arr3;
//}
//
//// Results set is arr1 - arr2, ie. whatever is in arr1 that is not in arr2
//// Time taken: n^2
//struct Array* Difference(struct Array arr1, struct Array arr2) {
//	int arr1_idx = 0;
//
//	struct Array* arr3 = (struct Array*)malloc(sizeof(struct Array)); // Create a new array in heap for the merged data
//	arr3->size = arr1.size;
//	arr3->length = 0;
//	arr3->A = (int*)malloc(arr3->size * sizeof(int));
//
//	for (; arr1_idx < arr1.length; arr1_idx++) {
//		if (LinearSearch(&arr2, arr1.A[arr1_idx]) == -1) { // If match not found
//			arr3->A[arr3->length++] = arr1.A[arr1_idx];
//		}
//	}
//
//	return arr3;
//}
//
//struct Array* DifferenceSorted(struct Array arr1, struct Array arr2) {
//	int arr1_idx = 0;
//	int arr2_idx = 0;
//
//	struct Array* arr3 = (struct Array*)malloc(sizeof(struct Array)); // Create a new array in heap for the merged data
//	arr3->size = arr1.size + arr2.size;
//	arr3->length = 0;
//	arr3->A = (int*)malloc(arr3->size * sizeof(int));
//
//	while (arr1_idx < arr1.length && arr2_idx < arr2.length) {
//		if (arr1.A[arr1_idx] < arr2.A[arr2_idx]) {
//			arr3->A[arr3->length++] = arr1.A[arr1_idx++];
//		}
//		else if (arr2.A[arr2_idx] < arr1.A[arr1_idx]) {
//			arr2_idx++;
//		}
//		else {
//			arr1_idx++;
//			arr2_idx++;
//		}
//	}
//
//	for (; arr1_idx < arr1.length; arr1_idx++) { // Vacuum up any dregs of array 1 and dump into array 3
//		arr3->A[arr3->length++] = arr1.A[arr1_idx];
//	}
//
//	return arr3;
//}
//
//int main() {
//
//	struct Array arr;
//	//int n;
//
//	//printf("\nEnter size of array: ");
//	//scanf_s("%d", &arr.size);	// Read array size from input
//
//	//arr.A = (int*)malloc(arr.size * sizeof(int)); // Create array in heap memory
//
//	//printf("\nEnter number of elements (length): ");
//	//scanf_s("%d", &arr.length);
//	//printf("\nEnter elements (separate each with a carriage return):\n");
//	//for (int i = 0; i < arr.length; i++) {
//	//	scanf_s("%d", &arr.A[i]);
//	//}
//	//printf("\n");
//
//	// Initialise a sorted list of 15 elements
//	//arr.size = 15;
//	//arr.length = 15;
//	//arr.A = (int*)malloc(arr.size * sizeof(int));
//	//arr.A[0] = 4; arr.A[1] = 8; arr.A[2] = 10; arr.A[3] = 15; arr.A[4] = 18; arr.A[5] = 21; arr.A[6] = 24; arr.A[7] = 27; arr.A[8] = 29; arr.A[9] = 33; arr.A[10] = 34; arr.A[11] = 37; arr.A[12] = 39; arr.A[13] = 41; arr.A[14] = 43;
//
//	// Initialise a list of 10 elements
//	arr.size = 10;
//	arr.length = 10;
//	arr.A = (int*)malloc(arr.size * sizeof(int));
//	// Sorted
//	arr.A[0] = 2; arr.A[1] = 3; arr.A[2] = 6; arr.A[3] = 7; arr.A[4] = 8; arr.A[5] = 9; arr.A[6] = 10; arr.A[7] = 12; arr.A[8] = 15; arr.A[9] = 99;
//	// Unsorted
//	// arr.A[0] = 8; arr.A[1] = 3; arr.A[2] = -9; arr.A[3] = -15; arr.A[4] = 6; arr.A[5] = -10; arr.A[6] = 7; arr.A[7] = -2; arr.A[8] = 12; arr.A[9] = -4;
//
//	int ch, x, index;
//
//	do {
//		printf("Menu\n");
//		printf("1. Insert\n");
//		printf("2. Delete\n");
//		printf("3. Search\n");
//		printf("4. Sum\n");
//		printf("5. Min\n");
//		printf("6. Max\n");
//		printf("7. Display\n");
//		printf("8. Exit\n\n");
//		printf("Enter your choice: ");
//		scanf_s("%d", &ch);
//		printf("\n");
//
//		switch (ch) {
//		case 1:
//			printf("Enter an element and index: ");
//			scanf_s("%d%d", &x, &index);
//			Insert(&arr, index, x);
//			break;
//		case 2:
//			printf("Enter index: ");
//			scanf_s("%d", &index);
//			Delete(&arr, index);
//			break;
//		case 3:
//			printf("Enter element to search: ");
//			scanf_s("%d", &x);
//			index = LinearSearch(&arr, x);
//			if (index > -1) {
//				printf("Element found at index %d\n\n", index);
//			}
//			else {
//				printf("Element not found\n\n");
//			}
//			break;
//		case 4:
//			printf("Sum is: %d\n\n", Sum(arr));
//			break;
//		case 5:
//			printf("Min is: %d\n\n", Min(arr));
//			break;
//		case 6:
//			printf("Max is: %d\n\n", Max(arr));
//			break;
//		case 7:
//			Display(arr);
//			break;
//		case 8:
//			break;
//		}
//	} while (ch < 8);
//
//	//Append(&arr, 123);
//
//	//printf("\n");
//	//printf("Loop-based Binary search\n");
//	//printf("Key = 18: Index = %d\n", BinarySearch(arr, 18));
//	//printf("Key = 34: Index = %d\n", BinarySearch(arr, 34));
//	//printf("Key = 25: Index = %d\n", BinarySearch(arr, 25));
//	//printf("\n");
//	//printf("Recursive Binary search\n");
//	//printf("Key = 18: Index = %d\n", RBinarySearch(arr.A, 0, arr.length, 18));
//	//printf("Key = 34: Index = %d\n", RBinarySearch(arr.A, 0, arr.length, 34));
//	//printf("Key = 25: Index = %d\n", RBinarySearch(arr.A, 0, arr.length, 25));
//
//	//printf("\n");
//	//Get(arr, 3);
//	//Get(arr, -17);
//	//Get(arr, 92);
//	//printf("\n");
//	//Set(&arr, 3, 123);
//	//Set(&arr, -17, 1);
//	//Set(&arr, 92, 1);
//	//printf("Max: %d\n", Max(arr));
//	//printf("Min: %d\n", Min(arr));
//	//printf("Sum: %d\n", Sum(arr));
//	//printf("Sum (recursive): %d\n", RSum(arr, arr.length - 1));
//	//printf("Average: %f\n", Average(arr));
//
//	//Reverse(&arr);
//
//	//int n = arr.length;
//	//for (int i = 0; i < n; i++) {
//	//	LeftRotate(&arr);
//	//	Display(arr);
//	//}
//	//for (int i = 0; i < n; i++) {
//	//	RightRotate(&arr);
//	//	Display(arr);
//	//}
//
//	//Delete(&arr, 0);
//	//InsertSort(&arr, -55);
//
//	//boolean result = IsSorted(arr);
//	//if (result == TRUE) {
//	//	printf("Array is sorted! :-D\n");
//	//}
//	//else {
//	//	printf("Array is not sorted :-(\n");
//	//}
//
//	//Rearrange(&arr);
//	
//	//struct Array arr2;
//	//arr2.size = 10;
//	//arr2.length = 10;
//	//arr2.A = (int*)malloc(arr2.size * sizeof(int));
//	//arr2.A[0] = -8; arr2.A[1] = -7; arr2.A[2] = -6; arr2.A[3] = -3; arr2.A[4] = -2; arr2.A[5] = 9; arr2.A[6] = 10; arr2.A[7] = 12; arr2.A[8] = 15; arr2.A[9] = 27;
//	//Display(arr2);
//
//	//struct Array *newarr = MergeArrays(arr, arr2);
//	
//	//struct Array* newarr = Union(arr, arr2);
//	//struct Array* newarr = UnionSorted(arr, arr2);
//	//struct Array* newarr = Intersection(arr, arr2);
//	//struct Array* newarr = IntersectionSorted(arr, arr2);
//	//struct Array* newarr = Difference(arr, arr2);
//	//struct Array* newarr = DifferenceSorted(arr, arr2);
//
//	//Display(*newarr);
//
//	return 0;
//}
