//#include <iostream>
//#include <ctime>
//
//using namespace std;
//
////typedef enum { TRUE = 0, FALSE = -1 } boolean;
//
//class Array {
//private:
//	int* A;	// An array of ints (size defined at initialisation)
//	int size;	// Max number of elements array can contain
//	int length;	// Number of populated elements in array
//	void Swap(int*, int*);
//public:
//	Array();
//	Array(int sz);
//	Array(const Array& old_arr); // Copy constructor
//	~Array();
//	void Display();
//	void Append(int);
//	void Insert(int, int);
//	int LinearSearch(int);
//	int BinarySearch(int);
//	//int RBinarySearch(int[], int, int, int);
//	int Get(int);
//	void Set(int, int);
//	int Max();
//	int Min();
//	int Sum();
//	float Average();
//	void Reverse();
//	void SwapReverse();
//	void InsertSort(int);
//	bool IsSorted();
//	void Rearrange();
//	Array* MergeArrays(Array &);
//	Array* UnionSorted(Array &);
//	Array* IntersectionSorted(Array &);
//	Array* DifferenceSorted(Array &);
//};
//
//
//Array::Array() {
//	size = 10;
//	length = 0;
//	A = new int[size];
//}
//
//Array::Array(int sz) {
//	size = sz;
//	length = 0;
//	A = new int[size];
//}
//
//Array::Array(const Array& old_arr) { // Copy constructor
//	size = old_arr.size;
//	length = old_arr.length;
//	A = new int[size];
//	for (int i = 0; i < length; i++) {
//		A[i] = old_arr.A[i];
//	}
//}
//
//Array::~Array() {
//	delete[] A;
//}
//// Simple swap of two elements
//void Array::Swap(int* x, int* y) {
//
//	int temp = *x;
//	*x = *y;
//	*y = temp;
//}
//
//// Display all the values of an array
//// Note array is call-by-value because array is not being modified
//void Array::Display() {
//	for (int i = 0; i < length; i++) {
//		cout << A[i] << " ";
//	}
//	cout << endl;
//}
//
//// Appends a given value to an array
//// Note array is call-by-reference because array is being modified
//void Array::Append(int elem) {
//	if (size <= length) {
//		cout << ("Error: Function Append() - Can't append any more elements. Array is full!\n\n");
//	}
//	else {
//		// cout << "Appending value " << elem << " at index " << length << endl;
//		A[length] = elem;
//		length++;
//	}
//}
//
//// Inserts a given value at a given index of an array
//void Array::Insert(int idx, int elem) {
//	if (size <= length) {
//		cout << ("Error: Function Insert() - Can't insert any more elements. Array is full!\n\n");
//	}
//	else if (idx > length) {
//		cout << "Error: Function Insert() - Can't insert element beyond current last element (length is " << length << ", requested index is " << idx << endl;
//	}
//	else if (idx < 0) {
//		cout << "Error: Function Insert() - Can't insert element at negative index " << idx << endl;
//	}
//	else {
//		cout << "Inserting value " << elem << " at index " << idx << endl;
//		for (int i = length; i > idx; i--) {
//			A[i] = A[i - 1];
//		}
//		A[idx] = elem;
//		length++;
//	}
//}
//
//// Returns the index of an array element that matches a given key
//int Array::LinearSearch(int key) {
//	for (int i = 0; i < length; i++) {
//		if (A[i] == key) {
//			// Optimisation method #1: move element back one place
//			//if (i > 0) {
//			//	swap(&arr->A[i], &arr->A[i - 1]);
//			//}
//
//			// Optimisation method #2: move element to head
//			// if (i > 0) {
//			//	Swap(&arr->A[0], &arr->A[i]);
//			// }
//			return i;
//		}
//	}
//
//	return -1; // Default, no match
//}
//
//// Binary search, use for sorted arrays
//int Array::BinarySearch(int key) {
//	int low = 0;
//	int high = length - 1;
//	int mid = 0;
//
//	while (low <= high) {
//		mid = (low + high) / 2;
//		if (key == A[mid]) { // Match
//			return mid;
//		}
//		else if (key < A[mid]) { // Key is in lower half of low..high
//			high = mid - 1;
//		}
//		else { // Key is in upper half of low..high
//			low = mid + 1;
//		}
//	}
//	return -1; // No match
//}
//
//// Recursive version of loop-based binary search
//// Note using a[] instead of the arr struct defined in main? I think the instructor did this
//// as it makes the function more generic.
////int Array::RBinarySearch(int a[], int low, int high, int key) {
////	if (low <= high) {
////		int mid = (low + high) / 2;
////		if (key == a[mid]) { // Match
////			return mid;
////		}
////		else if (key < a[mid]) { // Key is in lower half of low..high
////			return RBinarySearch(a, low, mid - 1, key);
////		}
////		else { // Key is in upper half of low..high
////			return RBinarySearch(a, mid + 1, high, key);
////		}
////	}
////	return -1; // No match
////}
//
//// Returns the value of an array at a given index
//int Array::Get(int idx) {
//	int index_value = -1;
//	if (idx >= length) {
//		cout << "Error: Function Get() - Index " << idx << " is greater than length of array (" << length << ")." << endl;
//	}
//	else if (idx < 0) {
//		cout << "Error: Function Get() - Index " << idx << " is less than zero. " << endl;
//	}
//	else {
//		index_value = A[idx];
//	}
//	return index_value;
//}
//
//// Set the index position of an array to a given value
//void Array::Set(int idx, int value) {
//	if (idx >= length) {
//		cout << "Error: Function Set() - Index " << idx << " is greater than length of array (" << length << ")." << endl;
//	}
//	else if (idx < 0) {
//		cout << "Error: Function Set() - Index " << idx << " is less than zero." << endl;
//	}
//	else {
//		cout << "Setting value at index " << idx << " to " << value << "." << endl;
//		A[idx] = value;
//	}
//}
//
//// Returns the min value of an array of ints
//// Time: 2n + 1 = O(n), could be very time costly!!!
//int Array::Min() {
//	int min = A[0];
//	for (int i = 1; i < length; i++) {
//		if (A[i] < min) {
//			min = A[i];
//		}
//	}
//	return min;
//}
//
//// Returns the max value of an array of ints
//// Time: 2n + 1 = O(n), could be very time costly!!!
//int Array::Max() {
//	int max = A[0];
//	for (int i = 1; i < length; i++) {
//		if (A[i] > max) {
//			max = A[i];
//		}
//	}
//	return max;
//}
//
//// Returns the sum value of an array of ints
//// Time: 2n + 3 = O(n)
//int Array::Sum() {
//	int sum = 0;
//	for (int i = 0; i < length; i++) {
//		sum += A[i];
//	}
//	return sum;
//}
//
//// Returns the average value of an array of ints
//// Time: 2n + 3 = O(n)
//float Array::Average() {
//	// Note that we already have a sum function, so why repeat the code?!
//	return (float)Sum() / length;
//}
//
//// Reverse the order of an array
//void Array::Reverse() {
//	Array *tmp_arr = new Array(size);
//
//	for (int i = length - 1, j = 0; i >= 0; i--, j++) {
//		tmp_arr->Append(A[i]);
//	}
//	for (int i = 0; i < length; i++) {
//		A[i] = tmp_arr->Get(i);
//	}
//}
//
//void Array::SwapReverse() {
//	for (int i = 0, j = length - 1; i < j; i++, j--) {
//		Swap(&A[i], &A[j]);
//	}
//}
//
//// Return true if array is sorted, otherwise return false
//bool Array::IsSorted() {
//	for (int i = 0; i < length - 1; i++) {
//		if (A[i] > A[i + 1]) {
//			return false;
//		}
//	}
//	return true;
//}
//
//// Insert an element in it's ordered place in a sorted array
//void Array::InsertSort(int elem) {
//	int i = length - 1;
//
//	if (length == size) { // No free space in array so can't insert
//		return;
//	}
//
//	while (i >= 0 && A[i] > elem) {
//		A[i + 1] = A[i];
//		i--;
//	}
//
//	A[i + 1] = elem;
//	length++;
//}
//
//void Array::Rearrange() {
//	int i = 0;
//	int j = length - 1;
//
//	while (i < j) {
//		while (A[i] < 0) {
//			i++;
//		}
//		while (A[j] >= 0) {
//			j--;
//		}
//		if (i < j) {
//			Swap(&A[i], &A[j]);
//		}
//	}
//}
//
//// Returns a sorted array that is a merger of two sorted arrays
//Array* Array::MergeArrays(Array &arr2) {
//	int i = 0; // arr1 index
//	int j = 0; // arr2 index
//
//	Array* arr3 = new Array(size + arr2.size); // Create a new array in heap for the merged data
//
//	while (i < length && j < arr2.length) {
//		if (A[i] <= arr2.A[j]) {
//			arr3->A[arr3->length++] = A[i++];
//		}
//		else {
//			arr3->A[arr3->length++] = arr2.A[j++];
//		}
//	}
//
//	for (; i < length; i++) {
//		arr3->A[arr3->length++] = A[i++];
//	}
//	for (; j < arr2.length; j++) {
//		arr3->A[arr3->length++] = arr2.A[j++];
//	}
//
//	return arr3;
//}
//
//// Returns an array that is the union of two sorted arrays
//// Time taken: Omega(m + n), or Omega(n)
//Array* Array::UnionSorted(Array &arr2) {
//	int arr1_idx = 0;
//	int arr2_idx = 0;
//
//	Array* arr3 = new Array(size + arr2.size); // Create a new array in heap for the merged data
//
//	while (arr1_idx < length && arr2_idx < arr2.length) {
//		if (A[arr1_idx] < arr2.A[arr2_idx]) { // Put elem from array 1 into array 3
//			arr3->A[arr3->length++] = A[arr1_idx++];
//		}
//		else if (arr2.A[arr2_idx] < A[arr1_idx]) { // Put elem from array 2 into array 3
//			arr3->A[arr3->length++] = arr2.A[arr2_idx++];
//		}
//		else {
//			arr3->A[arr3->length++] = A[arr1_idx++];
//			arr2_idx++; // Increment both arr1 and arr2 indexes as values are the same
//		}
//	}
//
//	for (; arr1_idx < length; arr1_idx++) { // Vacuum up any dregs of array 1 and dump into array 3
//		arr3->A[arr3->length++] = A[arr1_idx++];
//	}
//
//	for (; arr2_idx < arr2.length; arr2_idx++) { // Vacuum up any dregs of array 2 and dump into array 3
//		arr3->A[arr3->length++] = arr2.A[arr2_idx++];
//	}
//
//	return arr3;
//}
//
//Array* Array::IntersectionSorted(Array &arr2) {
//	int arr1_idx = 0;
//	int arr2_idx = 0;
//
//	Array* arr3 = new Array(size + arr2.size); // Create a new array in heap for the merged data
//
//	while (arr1_idx < length && arr2_idx < arr2.length) {
//		if (A[arr1_idx] < arr2.A[arr2_idx]) {
//			arr1_idx++;
//		}
//		else if (A[arr1_idx] > arr2.A[arr2_idx]) {
//			arr2_idx++;
//		}
//		else {
//			arr3->A[arr3->length++] = A[arr1_idx];
//			arr1_idx++;
//			arr2_idx++;
//		}
//	}
//
//	return arr3;
//}
//
//Array* Array::DifferenceSorted(Array &arr2) {
//	int arr1_idx = 0;
//	int arr2_idx = 0;
//
//	Array* arr3 = new Array(size + arr2.size); // Create a new array in heap for the merged data
//
//	while (arr1_idx < length && arr2_idx < arr2.length) {
//		if (A[arr1_idx] < arr2.A[arr2_idx]) {
//			arr3->A[arr3->length++] = A[arr1_idx++];
//		}
//		else if (arr2.A[arr2_idx] < A[arr1_idx]) {
//			arr2_idx++;
//		}
//		else {
//			arr1_idx++;
//			arr2_idx++;
//		}
//	}
//
//	for (; arr1_idx < length; arr1_idx++) { // Vacuum up any dregs of array 1 and dump into array 3
//		arr3->A[arr3->length++] = A[arr1_idx];
//	}
//
//	return arr3;
//}
//
//int main() {
//
//	cout << "Creating two arrays of randomly-generated ints..." << endl;
//
//	int size = 10;
//	int rand_val_1 = 0;
//	int rand_val_2 = 0;
//	Array arr1(size);
//	Array arr2(size);
//	//arr1.= new Array(size);
//	//Array* arr2 = new Array(size);
//	Array* arr3;
//	srand(100);
//
//	// Create two arrays of random ints, with each new number incremented from [1..10], thus creating two sorted arrays.
//	srand((unsigned int)time(NULL));
//	for (int i = 0; i < size; i++) {
//		rand_val_1 += (rand() % size + 1);
//		rand_val_2 += (rand() % size + 1);
//		arr1.Append(rand_val_1);
//		arr2.Append(rand_val_2);
//	}
//	arr1.Display();
//	arr2.Display();
//
//
//
//	int ch, x, index;
//	do {
//		cout << "Menu\n";
//		cout << "1. Insert\n";
//		cout << "2. Delete\n";
//		cout << "3. Search\n";
//		cout << "4. Min\n";
//		cout << "5. Max\n";
//		cout << "6. Sum\n";
//		cout << "7. Average\n";
//		cout << "8. Reverse\n";
//		cout << "9. Rearrange\n";
//		cout << "15. Merge\n";
//		cout << "16. Union\n";
//		cout << "17. Intersection\n";
//		cout << "18. Difference\n";
//		cout << "19. Display\n";
//		cout << "20. Exit\n\n";
//		cout << "Enter your choice: ";
//		//scanf_s("%d", &ch);
//		cin >> ch;
//		cout << endl;
//
//		switch (ch) {
//		case 1:
//			cout << "Enter an element to insert in arr1 and its' index: ";
//			//scanf_s("%d%d", &x, &index);
//			cin >> x >> index;
//			arr1.Insert(index, x);
//			break;
//		case 2:
//			//cout << ("Enter index: ");
//			//scanf_s("%d", &index);
//			//Delete(&arr, index);
//			cout << "***Delete() not implemented" << endl;
//			break;
//		case 3:
//			cout << "Enter element to search in arr1: ";
//			//scanf_s("%d", &x);
//			cin >> x;
//			index = arr1.LinearSearch(x);
//			if (index > -1) {
//				cout << "Element found at index " << index << endl;
//			}
//			else {
//				cout << "Element not found" << endl;
//			}
//			break;
//		case 4:
//			cout << "Min of arr1 is: " << arr1.Min() << "\n";
//			cout << "Min of arr2 is: " << arr2.Min() << endl << endl;
//			break;
//		case 5:
//			cout << "Max of arr1 is: " << arr1.Max() << "\n";
//			cout << "Max of arr2 is: " << arr2.Max() << "\n" << endl;
//			break;
//		case 6:
//			cout << "Sum of arr1 is: " << arr1.Sum() << "\n";
//			cout << "Sum of arr2 is: " << arr2.Sum() << "\n" << endl;
//			break;
//		case 7:
//			cout << "Average of arr1 is: " << arr1.Average() << "\n";
//			cout << "Average of arr2 is: " << arr2.Average() << "\n" << endl;
//			break;
//		case 8:
//			arr1.Reverse();
//			arr2.Reverse();
//			arr1.Display();
//			arr2.Display();
//			break;
//		case 9:
//			arr1.Rearrange();
//			arr2.Rearrange();
//			arr1.Display();
//			arr2.Display();
//			break;
//		case 15:
//			arr3 = arr1.MergeArrays(arr2);
//			arr3->Display();
//			delete arr3;
//			break;
//		case 16:
//			arr3 = arr1.UnionSorted(arr2);
//			arr3->Display();
//			delete arr3;
//			break;
//		case 17:
//			arr3 = arr1.IntersectionSorted(arr2);
//			arr3->Display();
//			delete arr3;
//			break;
//		case 18:
//			arr3 = arr1.DifferenceSorted(arr2);
//			arr3->Display();
//			delete arr3;
//			break;
//		case 19:
//			arr1.Display();
//			arr2.Display();
//			break;
//		case 20:
//			break;
//		}
//	} while (ch < 20);
//
//	//delete arr1;
//	//delete arr2;
//
//	return 0;
//}
