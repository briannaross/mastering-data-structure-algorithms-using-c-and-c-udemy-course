//#include <iostream>
//
//using namespace std;
//
//// This can be represented as either upper triangle or lower triangle,
//// row-major or column major. This example is lower triangle row-major.
//// The only change is in the Get() function, where for all i > j the
//// result of the reverse of the cell formula for all i <= j is returned.
//class LowTriSymMatrix {
//private:
//	int *A;
//	int size;
//public:
//	LowTriSymMatrix();
//	LowTriSymMatrix(int);
//	~LowTriSymMatrix();
//	void Set(int, int, int);
//	int Get(int, int);
//	int GetDims();
//	void Display();
//};
//
//LowTriSymMatrix::LowTriSymMatrix() {
//	size = 2;
//	A = new int[2 * (2 + 1) / 2];
//}
//
//LowTriSymMatrix::LowTriSymMatrix(int s) {
//	size = s;
//	A = new int[(int)(size * (size + 1) / 2)];
//}
//
//LowTriSymMatrix::~LowTriSymMatrix() {
//	delete[] A;
//}
//
//void LowTriSymMatrix::Set(int i, int j, int elem) {
//	if (i >= j) {
//		A[i * (i - 1) / 2 + j - 1] = elem;
//	}
//}
//
//int LowTriSymMatrix::Get(int i, int j) {
//	if (i >= j) {
//		return A[i * (i - 1) / 2 + j - 1];
//	}
//
//	return A[j * (j - 1) / 2 + i - 1];
//}
//
//int LowTriSymMatrix::GetDims() {
//	return size;
//}
//
//void LowTriSymMatrix::Display() {
//	for (int i = 1; i <= size; i++) {
//		for (int j = 1; j <= size; j++) {
//			cout << Get(i, j) << " ";
//		}
//		cout << endl;
//	}
//}
//
//int main() {
//	int dims;
//	cout << "Enter dimensions: ";
//	cin >> dims;
//
//	LowTriSymMatrix m(dims);
//
//	int elem;
//	cout << "Enter all elements: " << endl;
//
//	for (int i = 1; i <= dims; i++) {
//		for (int j = 1; j <= dims; j++) {
//			cin >> elem;
//			m.Set(i, j, elem);
//		}
//	}
//	cout << "\n\n";
//
//	m.Display();
//
//	return 0;
//}
