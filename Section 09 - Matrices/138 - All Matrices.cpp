//#include <iostream>
//
//using namespace std;
//
//enum mType {
//	DIAGONAL,
//	LOWER_TRIANGULAR,
//	UPPER_TRIANGULAR,
//	SYMMETRIC,
//	TRI_DIAGONAL,
//	TOEPLITZ
//};
//
//class Matrix {
//private:
//	int* A;
//	int size;	// Matrix is size * size 
//	int type;	// Type of matrix
//	int GetArraySize();
//public:
//	Matrix();
//	Matrix(int, int);
//	~Matrix();
//	int SetValueAt(int, int, int);
//	int GetValueAt(int, int);
//	int GetDims();
//	void Display();
//};
//
//Matrix::Matrix() {
//	size = 3;
//	type = DIAGONAL;
//	A = new int[size];
//}
//
//Matrix::Matrix(int s, int t) {
//	size = s;
//	type = t;
//	A = new int[GetArraySize()];
//}
//
//Matrix::~Matrix() {
//	delete[] A;
//}
//
//int Matrix::GetArraySize() {
//	switch (type) {
//	case DIAGONAL:
//		return size;
//		break;
//	case LOWER_TRIANGULAR:
//		return size * (size + 1) / 2;
//		break;
//	case UPPER_TRIANGULAR:
//		return size * (size + 1) / 2;
//		break;
//	case SYMMETRIC:
//		return size * (size + 1) / 2;
//		break;
//	case TRI_DIAGONAL:
//		return (3 * size) - 2;
//		break;
//	case TOEPLITZ:
//		return (2 * size) - 1;
//		break;
//	default:
//		break;
//	}
//
//	return size;
//}
//
//int Matrix::SetValueAt(int i, int j, int value) {
//	if (i < 1 || i > size || j < 1 || j > size) {
//		return -1;
//	}
//
//	switch (type) {
//	case DIAGONAL:
//		if (i == j) {
//			A[i - 1] = value;
//		}
//		break;
//	case LOWER_TRIANGULAR:
//		if (i >= j) {
//			A[i * (i - 1) / 2 + j - 1] = value;
//		}
//		break;
//	case UPPER_TRIANGULAR:
//		if (i <= j) {
//			A[size * (i - 1) - (i - 2) * (i - 1) / 2 + j - i] = value;
//		}
//		break;
//	case SYMMETRIC:
//		if (i >= j) {
//			A[i * (i - 1) / 2 + j - 1] = value;
//		}
//		break;
//	case TRI_DIAGONAL:
//		if (i - j == 1) {
//			A[i - 2] = value;
//		}
//		else if (i - j == 0) {
//			A[size - 1 + i - 1] = value;
//		}
//		else if (i - j == -1) {
//			A[(2 * size) - 1 + i - 1] = value;
//		}
//		break;
//	case TOEPLITZ:
//		if (i <= j) {
//			A[j - i] = value;
//		}
//		else {
//			A[size + i - j - 1] = value;
//		}
//		break;
//	default:
//		break;
//	}
//}
//
//int Matrix::GetValueAt(int i, int j) {
//	if (i < 1 || i > size || j < 1 || j > size) {
//		return -1;
//	}
//
//	switch (type) {
//	case DIAGONAL:
//		if (i == j) {
//			return A[i - 1];
//		}
//		else {
//			return 0;
//		}
//		break;
//	case LOWER_TRIANGULAR:
//		if (i >= j) {
//			return A[i * (i - 1) / 2 + j - 1];
//		}
//		else {
//			return 0;
//		}
//		break;
//	case UPPER_TRIANGULAR:
//		if (i <= j) {
//			return A[size * (i - 1) - (i - 2) * (i - 1) / 2 + j - i];
//		}
//		else {
//			return 0;
//		}
//		break;
//	case SYMMETRIC:
//		if (i >= j) {
//			return A[i * (i - 1) / 2 + j - 1];
//		}
//		else {
//			return A[j * (j - 1) / 2 + i - 1];
//		}
//		break;
//	case TRI_DIAGONAL:
//		if (i - j == 1) {
//			return A[i - 2];
//		}
//		else if (i - j == 0) {
//			return A[size - 1 + i - 1];
//		}
//		else if (i - j == -1) {
//			return A[(2 * size) - 1 + i - 1];
//		}
//		else {
//			return 0;
//		}
//		break;
//	case TOEPLITZ:
//		if (i <= j) {
//			return A[j - i];
//		}
//		else {
//			return A[size + i - j - 1];
//		}
//		break;
//	default:
//		break;
//	}
//}
//
//int Matrix::GetDims() {
//	return size;
//}
//
//void Matrix::Display() {
//	for (int i = 1; i <= size; i++) {
//		for (int j = 1; j <= size; j++) {
//			cout << GetValueAt(i, j) << " ";
//		}
//		cout << endl;
//	}
//}
//
//int main() {
//	int dims = 0;
//	int choice = 0;
//	int i = 0;
//	int j = 0;
//	int result = 0;
//	int value = 0;
//
//	cout << "Matrices\n";
//	cout << "========\n\n";
//	cout << "Enter dimensions: ";
//	cin >> dims;
//
//	do {
//		cout << "\n\nMatrix types\n";
//		cout << "------------\n";
//		cout << "1) Diagonal\n";
//		cout << "2) Lower Triangular\n";
//		cout << "3) Upper Triangular\n";
//		cout << "4) Symmetric\n";
//		cout << "5) Tri-Diagonal\n";
//		cout << "6) Toeplitz\n";
//		cout << "Enter choice [1-6]: ";
//		cin >> choice;
//		choice--; // Because enum for matrices starts at 0, not 1 as in the menu options.
//	} while (choice < DIAGONAL || choice > TOEPLITZ);
//
//	Matrix m(dims, choice);
//
//	cout << "Enter all values: " << endl;
//	for (int i = 1; i <= dims; i++) {
//		for (int j = 1; j <= dims; j++) {
//			cin >> value;
//			m.SetValueAt(i, j, value);
//		}
//	}
//
//	do {
//		cout << "\n\nMenu\n";
//		cout << "====\n";
//		cout << "1. Get\n";
//		cout << "2. Set\n";
//		cout << "3. Display\n";
//		cout << "4. Exit\n\n";
//		cout << "Enter your choice: ";
//		cin >> choice;
//		cout << endl;
//
//		switch (choice) {
//		case 1:
//			do {
//				cout << "Enter row and column of element to get: ";
//				cin >> i >> j;
//				result = m.GetValueAt(i, j);
//				if (result == -1) {
//					cout << "Invalid coordinates\n";
//				}
//				else {
//					cout << "Value: " << result << endl;
//				}
//			} while (result == -1);
//			break;
//		case 2:
//			do {
//				cout << "Enter row, column, and value of element to set: ";
//				cin >> i >> j >> value;
//				result = m.SetValueAt(i, j, value);
//				if (result == -1) {
//					cout << "Invalid coordinates\n";
//				}
//			} while (result == -1);
//			break;
//		case 3:
//			m.Display();
//			break;
//		case 4:
//		default:
//			break;
//		}
//	} while (choice != 4);
//
//	return 0;
//}
