//#include <iostream>
//
//using namespace std;
//
//class UpperTriMatrix {
//private:
//	int *A;
//	int size;
//public:
//	UpperTriMatrix();
//	UpperTriMatrix(int);
//	~UpperTriMatrix();
//	void Set(int, int, int);
//	int Get(int, int);
//	int GetDims();
//	void Display();
//};
//
//UpperTriMatrix::UpperTriMatrix() {
//	size = 2;
//	A = new int[2 * (2 + 1) / 2];
//}
//
//UpperTriMatrix::UpperTriMatrix(int s) {
//	size = s;
//	A = new int[(int)(size * (size + 1) / 2)];
//}
//
//UpperTriMatrix::~UpperTriMatrix() {
//	delete[] A;
//}
//
//void UpperTriMatrix::Set(int i, int j, int elem) {
//	if (i <= j) {
//		A[size * (i - 1) - (i - 2) * (i - 1) / 2 + j - i] = elem;
//	}
//}
//
//int UpperTriMatrix::Get(int i, int j) {
//	if (i <= j) {
//		return A[size * (i - 1) - (i - 2) * (i - 1) / 2 + j - i];
//	}
//
//	return 0;
//}
//
//int UpperTriMatrix::GetDims() {
//	return size;
//}
//
//void UpperTriMatrix::Display() {
//	for (int i = 1; i <= size; i++) {
//		for (int j = 1; j <= size; j++) {
//			cout << Get(i, j) << " ";
//		}
//		cout << endl;
//	}
//}
//
//int main() {
//	int dims;
//	cout << "Enter dimensions: ";
//	cin >> dims;
//
//	UpperTriMatrix m(dims);
//
//	int elem;
//	cout << "Enter all elements: " << endl;
//
//	for (int i = 1; i <= dims; i++) {
//		for (int j = 1; j <= dims; j++) {
//			cin >> elem;
//			m.Set(i, j, elem);
//		}
//	}
//	cout << "\n\n";
//
//	m.Display();
//
//	return 0;
//}
