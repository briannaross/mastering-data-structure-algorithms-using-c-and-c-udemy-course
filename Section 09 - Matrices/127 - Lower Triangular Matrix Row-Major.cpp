//#include <iostream>
//
//using namespace std;
//
//class LowTriMatrix {
//private:
//	int *A;
//	int size;
//public:
//	LowTriMatrix();
//	LowTriMatrix(int);
//	~LowTriMatrix();
//	void Set(int, int, int);
//	int Get(int, int);
//	int GetDims();
//	void Display();
//};
//
//LowTriMatrix::LowTriMatrix() {
//	size = 2;
//	A = new int[2 * (2 + 1) / 2];
//}
//
//LowTriMatrix::LowTriMatrix(int s) {
//	size = s;
//	A = new int[(int)(size * (size + 1) / 2)];
//}
//
//LowTriMatrix::~LowTriMatrix() {
//	delete[] A;
//}
//
//void LowTriMatrix::Set(int i, int j, int elem) {
//	if (i >= j) {
//		A[i * (i - 1) / 2 + j - 1] = elem;
//	}
//}
//
//int LowTriMatrix::Get(int i, int j) {
//	if (i >= j) {
//		return A[i * (i - 1) / 2 + j - 1];
//	}
//
//	return 0;
//}
//
//int LowTriMatrix::GetDims() {
//	return size;
//}
//
//void LowTriMatrix::Display() {
//	for (int i = 1; i <= size; i++) {
//		for (int j = 1; j <= size; j++) {
//			cout << Get(i, j) << " ";
//		}
//		cout << endl;
//	}
//}
//
//int main() {
//	int dims;
//	cout << "Enter dimensions: ";
//	cin >> dims;
//
//	LowTriMatrix m(dims);
//
//	int elem;
//	cout << "Enter all elements: " << endl;
//
//	for (int i = 1; i <= dims; i++) {
//		for (int j = 1; j <= dims; j++) {
//			cin >> elem;
//			m.Set(i, j, elem);
//		}
//	}
//	cout << "\n\n";
//
//	m.Display();
//
//	return 0;
//}
