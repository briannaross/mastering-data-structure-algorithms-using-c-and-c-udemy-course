#include <stdio.h>
#include <stdlib.h>

struct Element {
	int i;
	int j;
	int x;
};

struct Sparse {
	int m;
	int n;
	int num;
	struct Element* e;
};

void Create(struct Sparse* s) {
	printf("Enter dimensions: ");
	scanf_s("%d%d", &s->m, &s->n);
	printf("Enter number of non-zero elements: ");
	scanf_s("%d", &s->num);
	s->e = (struct Element*)malloc(s->num * sizeof(struct Element));
	if (s->e) {
		printf("Enter all elements: ");
		for (int i = 0; i < s->num; i++) {
			scanf_s("%d%d%d", &s->e[i].i, &s->e[i].j, &s->e[i].x);
		}
	}
}

struct Sparse* Add(struct Sparse s1, struct Sparse s2) {
	//if (s1.m != s2.m || s1.n != s2.n) {
	//	return 0;
	//}
	int i = 0;
	int j = 0;
	int k = 0;

	struct Sparse* sum = (struct Sparse*)malloc(sizeof(struct Sparse));
	if (sum) {
		sum->e = (struct Element*)malloc((s1.num + s2.num) * sizeof(struct Element)); // Total elems (s1 + s2)
		if (sum->e) {
			while (i < s1.num && j < s2.num) {
				// Check rows
				if (s1.e[i].i < s2.e[j].i) {
					sum->e[k++] = s1.e[i++];
				}
				else if (s1.e[i].i > s2.e[j].i) {
					sum->e[k++] = s2.e[j++];
				}
				else {
					// Check columns
					if (s1.e[i].j < s2.e[j].j) {
						sum->e[k++] = s1.e[i++];
					}
					else if (s1.e[i].j > s2.e[j].j) {
						sum->e[k++] = s2.e[j++];
					}
					// We have a match!
					else {
						sum->e[k].i = s1.e[i].i;
						sum->e[k].j = s1.e[i].j;
						sum->e[k++].x = s1.e[i++].x + s2.e[j++].x;
					}
				}
			}

			// Copy any remaining elements of first matrix into sum
			for (; i < s1.num; i++) {
				sum->e[k++] = s1.e[i];
			}

			// Copy any remaining elements of second matrix into sum
			for (; j < s2.num; j++) {
				sum->e[k++] = s2.e[j];
			}

			sum->m = s1.m;	// Cols (s1 and s2 are the same size)
			sum->n = s1.n;	// Rows (s1 and s2 are the same size)
			sum->num = k;	// Total number of elements
		}
	}

}

void Display(struct Sparse s) {
	int k = 0;
	for (int i = 0; i < s.m; i++) {
		for (int j = 0; j < s.n; j++) {
			if (i == s.e[k].i && j == s.e[k].j) {
				printf("%d ", s.e[k++].x);
			}
			else {
				printf("0 ");
			}
		}
		printf("\n");
	}
}

int main() {
	struct Sparse s1, s2, *s3;

	Create(&s1);
	Create(&s2);

	s3 = Add(s1, s2);

	printf("First Matrix\n");
	Display(s1);

	printf("Second Matrix\n");
	Display(s2);

	printf("Sum of Matrixes\n");
	Display(*s3);

	return 0;
}