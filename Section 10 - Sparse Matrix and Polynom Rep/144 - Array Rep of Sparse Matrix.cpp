//#include <iostream>
//
//using namespace std;
//
//struct Element {
//	int i;
//	int j;
//	int x;
//};
//
//class Sparse {
//private:
//	int m;
//	int n;
//	int num;
//	struct Element* e;
//public:
//	Sparse(int, int, int);
//	~Sparse();
//	Sparse(const Sparse& s);
//	Sparse& operator=(const Sparse&);
//	Sparse operator+(Sparse&);
//	friend istream& operator>>(istream&, Sparse&);
//	friend ostream& operator<<(ostream&, Sparse&);
//};
//
//Sparse::Sparse(int m, int n, int num) {
//	this->m = m;
//	this->n = n;
//	this->num = num;
//	e = new Element[this->num];
//}
//
//Sparse::~Sparse() {
//	delete[] e;
//}
//
//Sparse::Sparse(const Sparse& s) {
//	m = s.m;
//	n = s.n;
//	num = s.num;
//	e = new Element[num];
//	for (int i = 0; i < num; i++) {
//		e[i] = s.e[i];
//	}
//}
//
//Sparse& Sparse::operator= (const Sparse& s)
//{
//	if (this == &s) {
//		return *this;
//	}
//
//	if (e) {
//		delete[] e;
//	}
//
//	m = s.m;
//	n = s.n;
//	num = s.num;
//
//	e = new Element[num];
//
//	for (int i = 0; i < num; i++) {
//		e[i] = s.e[i];
//	}
//
//	return *this;
//}
//
//Sparse Sparse::operator+(Sparse& s) {
//	if (m != s.m || n != s.n) {
//		return Sparse(0, 0, 0);
//	}
//
//	Sparse* sum = new Sparse(m, n, num + s.num);
//	int i = 0;
//	int j = 0;
//	int k = 0;
//
//	// Copy elems into sum array, add where coords match.
//	while (i < num && j < s.num) {
//		// Copy where mismatch on row
//		if (e[i].i < s.e[j].i) {
//			sum->e[k++] = e[i++];
//		}
//		else if (e[i].i > s.e[j].i) {
//			sum->e[k++] = s.e[j++];
//		}
//		else {
//			// Copy where mismatch on col
//			if (e[i].j < s.e[j].j) {
//				sum->e[k++] = e[i++];
//			}
//			else if (e[i].j > s.e[j].j) {
//				sum->e[k++] = s.e[j++];
//			}
//			// We have a match, so add.
//			else {
//				sum->e[k].i = e[i].i;
//				sum->e[k].j = e[i].j;
//				sum->e[k++].x = e[i++].x + s.e[j++].x;
//			}
//		}
//	}
//
//	// Copy any remaining elements of first matrix into sum
//	for (; i < num; i++) {
//		sum->e[k++] = e[i];
//	}
//
//	// Copy any remaining elements of second matrix into sum
//	for (; j < s.num; j++) {
//		sum->e[k++] = s.e[j];
//	}
//
//	sum->m = m;	// Cols (s1 and s2 are the same size)
//	sum->n = n;	// Rows (s1 and s2 are the same size)
//	sum->num = k;	// Total number of elements
//
//	return *sum;
//}
//
//istream& operator>>(istream& is, Sparse& s) {
//	cout << "Enter all elements:\n";
//	for (int i = 0; i < s.num; i++) {
//		cin >> s.e[i].i >> s.e[i].j >> s.e[i].x;
//	}
//	return is;
//}
//
//ostream& operator<<(ostream& os, Sparse& s) {
//	int k = 0;
//	for (int i = 0; i < s.m; i++) {
//		for (int j = 0; j < s.n; j++) {
//			if (i == s.e[k].i && j == s.e[k].j) {
//				cout << s.e[k++].x << " ";
//			}
//			else {
//				cout << "0 ";
//			}
//		}
//		cout << endl;
//	}
//	return os;
//}
//
//int main() {
//	Sparse s1(3, 3, 3);
//	Sparse s2(3, 3, 3);
//
//	cin >> s1;
//	cin >> s2;
//
//	Sparse sum = s1 + s2;
//
//	cout << "\nFirst Matrix" << endl;
//	cout << s1;
//	cout << "\nSecond Matrix" << endl;
//	cout << s2;
//	cout << "\nSum Matrix" << endl;
//	cout << sum;
//
//	return 0;
//}
