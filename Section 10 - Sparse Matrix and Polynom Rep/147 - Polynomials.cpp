//#include <iostream>
//#include <math.h>
//
//using namespace std;
//
//struct Term {
//	int coeff;
//	int exp;
//};
//
//struct Poly {
//	int n;
//	struct Term* terms;
//};
//
//int SumPoly(struct Poly p, int x) {
//	int sum = 0;
//
//	for (int i = 0; i < p.n; i++) {
//		sum += (p.terms[i].coeff * pow(x, p.terms[i].exp));
//	}
//
//	return sum;
//}
//
//struct Poly* AddPoly(struct Poly p1, struct Poly p2) {
//	Poly *p3 = new Poly;
//	int n = p1.n + p2.n;
//	p3->terms = new Term[n];
//	int i = 0;
//	int j = 0;
//	int k = 0;
//
//	while (i < p1.n && j < p2.n) {
//		if (p1.terms[i].exp > p2.terms[j].exp) {
//			p3->terms[k++] = p1.terms[i++];
//		}
//		else if (p1.terms[i].exp < p2.terms[j].exp) {
//			p3->terms[k++] = p2.terms[j++];
//		}
//		else {
//			p3->terms[k].exp = p1.terms[i].exp;
//			p3->terms[k++].coeff = p1.terms[i++].coeff + p2.terms[j++].coeff;
//		}
//	}
//
//	for (; i < p1.n; i++) {
//		p3->terms[k++] = p1.terms[i];
//	}
//
//	for (; j < p2.n; j++) {
//		p3->terms[k++] = p2.terms[j];
//	}
//
//	p3->n = k;
//
//	return p3;
//}
//
//void Create(struct Poly *p) {
//	cout << "Enter number of non-zero terms: ";
//	cin >> p->n;
//	p->terms = new Term[p->n];
//
//	int sum = 0;
//
//	cout << "Enter terms (polynomial coefficient): \n";
//	for (int i = 0; i < p->n; i++) {
//		cout << "Term " << i + 1 << ") : ";
//		cin >> p->terms[i].coeff >> p->terms[i].exp;
//	}
//
//}
//
//void Display(struct Poly p) {
//	for (int i = 0; i < p.n; i++) {
//		cout << p.terms[i].coeff << "x" << p.terms[i].exp << " ";
//		if (i < p.n - 1) {
//			cout << "+ ";
//		}
//	}
//	cout << endl;
//}
//
//int main() {
//	Poly p1;
//	Poly p2;
//	Poly* p3;
//	int x = 5;
//
//	cout << "Polynomial Program\n";
//	cout << "==================\n";
//	cout << endl;
//
//	Create(&p1);
//
//	Create(&p2);
//
//	p3 = AddPoly(p1, p2);
//
//	Display(p1);
//	Display(p2);
//	Display(*p3);
//
//	//cout << "Sum: " << SumPoly(p1, x) << endl;
//	
//
//	return 0;
//}