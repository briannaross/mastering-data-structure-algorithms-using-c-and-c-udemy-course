#include <iostream>

using namespace std;

//template<class T>
class Array {
private:
	int size;
	int length;
	int min;
	int max;
	//T* A;
	int* A;
public:
	Array() {
		size = 10;
		length = 0;
		min = 0;
		max = 0;
		//A = new T[size];
		A = new int[size];
	}
	Array(int sz) {
		size = sz;
		length = 0;
		min = 0;
		max = 0;
		//A = new T[size];
		A = new int[size];
	}
	~Array() {
		delete[] A;
	}

	void Display();
	//void Insert(int idx, T elem);
	void Insert(int idx, int elem);
	//T Delete(int idx);
	int Delete(int idx);
	void FindMissingLoop();
	void FindMissingElems(int low, int high);
	void FindMissingElemsSorted(int low, int high);
	void FindDuplicates();
	void FindDuplicatesUsingHash();
	void FindPairWithSum(int);
	void FindPairWithSumUsingHash(int);
	void FindPairWithSumSorted(int);
	void FindMinMax();
};

//template <class T>
//void Array<T>::Display() {
void Array::Display() {
	for (int i = 0; i < length; i++) {
		cout << A[i] << " ";
	}
	cout << endl;
}

//template <class T>
//void Array<T>::Insert(int idx, T elem) {
void Array::Insert(int idx, int elem) {
	if (idx >= 0 && idx <= length) {
		for (int i = length - 1; i >= idx; i--) {
			A[i + 1] = A[i];
		}
		A[idx] = elem;
		length++;

		if (length == 1) {
			min = max = elem;
		}
		else {
			if (elem > max) {
				max = elem;
			}
			else if (elem < min) {
				min = elem;
			}
		}
	}
}

//template <class T>
//T Array<T>::Delete(int idx) {
int Array::Delete(int idx) {
	int elem = -1;
	if (idx >= 0 && idx < length) {
		elem = A[idx];
		for (int i = idx; i < length - 1; i++) {
			A[i] = A[i + 1];
		}
		length--;
	}
	return elem;
}


void Array::FindMissingLoop() {
	int sum = 0;
	int expected = 0;

	for (int i = 0; i < length; i++) {
		sum += A[i];
	}

	expected = (A[length - 1] * (A[length - 1] + 1)) / 2;

	cout << expected - sum << endl;
}

void Array::FindMissingElems(int low, int high) {
	int difference = low;
	int lengthBitset = high - low + 1;
	bool* bitset = new bool[lengthBitset];
	for (int i = 0; i < lengthBitset; i++) {
		bitset[i] = false;
	}

	for (int i = 0; i < length; i++) {
		int idx = A[i] - difference;
		bitset[idx] = true;
	}

	for (int i = 1; i < lengthBitset; i++) {
		if (bitset[i] == false) {
			cout << "Missing element: " << i + difference << endl;
		}
	}

	delete[] bitset;
}
void Array::FindMissingElemsSorted(int low, int high) {
	int i = 0;
	int difference = low - i;

	while (i < length) {
		if (A[i] - i != difference) {
			cout << "Missing element: " << i + difference << endl;
			difference++;
		}
		else { // Increment i if no change in difference
			i++;
		}
	}
}

void Array::FindDuplicates() {
	int lastDuplicate = 0;
	int numLastDuplicate = 0;
	int numDuplicates = 0;

	for (int i = 0; i < length - 1; i++) {
		if (A[i] == A[i + 1]) { // Found a duplicate
			if (A[i] != lastDuplicate) { // This duplicate is new
				lastDuplicate = A[i];
			}
			numLastDuplicate++;
		}
		else if (numLastDuplicate > 0) { // Means we've finished with the current set of duplicates
			cout << "Value " << lastDuplicate << " appears " << numLastDuplicate + 1 << " times." << endl;
			numLastDuplicate = 0;
			numDuplicates++;
		}
	}

	if (numLastDuplicate > 0) { // Means we've finished with the current set of duplicates
		cout << "Value " << lastDuplicate << " appears " << numLastDuplicate + 1 << " times." << endl;
		numDuplicates++;
	}

	cout << "Number of duplicate values found: " << numDuplicates << endl;
}

// Find duplicates and number of them in a sorted or unsorted array of any size
// This might be inefficient for massive ranges.
void Array::FindDuplicatesUsingHash() {
	// Interesting thought: If sqrt(range) > length^2, would it be quicker to do the first procedure in the video (video 106)?

	int numDuplicates = 0;
	int lengthHash = max - min + 1;
	int* hash = new int[lengthHash];
	fill_n(hash, lengthHash, 0); // Rapid initialisation

	for (int i = 0; i < length; i++) {
		hash[A[i] - min]++;
	}

	for (int i = 0; i < lengthHash; i++) {
		if (hash[i] > 1) {
			cout << "Value " << i + min << " appears " << hash[i] << " times." << endl;
			numDuplicates++;
		}
	}

	cout << "Number of duplicate values found: " << numDuplicates << endl;

	delete[] hash;
}

void Array::FindPairWithSum(int sum) {
	cout << "Element pairs that sum to " << sum << endl;
	for (int i = 0; i < length - 1; i++) {
		for (int j = i + 1; j < length; j++) {
			if ((A[i] + A[j]) == sum) {
				cout << "A[" << i << "]: " << A[i] << " + A[" << j << "]: " << A[j] << "\n";
			}
		}
	}
}

void Array::FindPairWithSumUsingHash(int sum) {
	int lengthHash = 100;
	int* hash = new int[lengthHash];
	fill_n(hash, lengthHash, 0); // Rapid initialisation

	for (int i = 0; i < length; i++) {
		hash[A[i]]++;
	}

	for (int i = 0; i < length; i++) {
		if (hash[sum - A[i]] != 0) {
			cout << "A[" << i << "]: " << A[i] << " + A[" << sum - A[i] << "]: " << sum - A[i] << "\n";
		}
	}
}

void Array::FindPairWithSumSorted(int sum) {
	// My solution...
	//int iters = 0;
	//int calcs = 0;

	//for (int i = 0; i < length - 1 && (A[i] + A[i + 1] <= sum); i++) {
	//	for (int j = i + 1; j < length; j++) {
	//		if (A[i] + A[j] == sum) {
	//			cout << "Found match: A[" << i << "] (" << A[i] << ") + A[" << j << "] (" << A[j] << ") = " << sum << endl;
	//		}
	//	}
	//}

	// ...but the tutor's solution is better!
	int low = 0;
	int high = length - 1;

	while (low < high) {
		if (A[low] + A[high] > sum) {
			high--;
		}
		else if (A[low] + A[high] < sum) {
			low++;
		}
		else {
			cout << "Found match: A[" << low << "] (" << A[low] << ") + A[" << high << "] (" << A[high] << ") = " << sum << endl;
			low++;
			high--;
		}
	}
}

// Best case time: n - 1
// Worst case time: 2(n - 1)
// Time take: O(n)
void Array::FindMinMax() {
	int min = A[0];
	int max = A[0];

	for(int i = 1; i < length; i++) {
		if (A[i] > max) {
			max = A[i];
		}
		else if (A[i] < min) {
			min = A[i];
		}
	}

	cout << "Min: " << min << "\nMax: " << max << endl;
}

int main() {

	//Array<int> arr(11);
	Array arr(10);

	arr.Insert(0, 5);
	arr.Insert(1, 8);
	arr.Insert(2, 3);
	arr.Insert(3, 9);
	arr.Insert(4, 6);
	arr.Insert(5, 2);
	arr.Insert(6, 10);
	arr.Insert(7, 7);
	arr.Insert(8, -1);
	arr.Insert(9, 4);
	arr.Display();

	//arr.FindMissingLoop();

	//arr.FindMissingElems(1, 12);
	//arr.FindMissingElemsSorted(0, 12);
	//arr.FindDuplicates();
	//arr.FindDuplicatesUsingHash();
	//arr.FindPairWithSum(9);
	//arr.FindPairWithSumUsingHash(9);
	//arr.FindPairWithSumSorted(10);
	arr.FindMinMax();


	return 0;
}