//#include <stdio.h>
//
//// Note that n! = 1 for n = 0.
//
//// Time = O(n), Space = O(n)
//int recursive_factorial(int n) {
//
//	if (n == 0) {
//		return 1;
//	}
//
//	return recursive_factorial(n - 1) * n;
//}
//
//int loop_factorial(int n) {
//	int s = 1;
//	for (int i = 1; i <= n; i++) {
//		s *= i;
//	}
//	return s;
//}
//
//int main() {
//
//	printf("%d\n", recursive_factorial(0));
//
//	printf("%d\n", loop_factorial(0));
//
//
//	return 0;
//}
