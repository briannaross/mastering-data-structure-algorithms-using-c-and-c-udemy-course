//#include <stdio.h>
//
//// Time = O(n), Space = O(n) (least efficient as it creates a new function entry on the stack on each iteration)
//int recursive_sum(int n) {
//	if (n > 0) {
//		return n + recursive_sum(n - 1);
//	}
//
//	return n;
//}
//
//// Time = O(1) (most efficient)
//int formula_sum(int n) {
//	return (n * (n + 1)) / 2;
//}
//
//// Time = O(n)
//int loop_sum(int n) {
//	int s = 0;
//	for (int i = 0; i <= n; i++) {
//		s += i;
//	}
//	return s;
//}
//
//int main() {
//
//	printf("%d\n", recursive_sum(10));
//
//	printf("%d\n", formula_sum(10));
//
//	printf("%d\n", loop_sum(10));
//
//	return 0;
//}
