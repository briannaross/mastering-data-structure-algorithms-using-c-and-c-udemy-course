//#include <stdio.h>
//
//// Time: O(2^n)
//void TOH(int n, int src, int inter, int dest) {
//	static int calls = 0;
//
//	if (n > 0) {
//		TOH(n - 1, src, dest, inter);
//		printf("%d: Moving from %d to %d.\n", ++calls, src, dest);
//		TOH(n - 1, inter, src, dest);
//	}
//
//	return;
//}
//
//int main() {
//
//	TOH(4, 1, 2, 3);
//
//}
