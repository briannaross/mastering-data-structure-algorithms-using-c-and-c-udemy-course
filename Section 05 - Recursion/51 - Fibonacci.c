//#include <stdio.h>
//
//// Time: O(2^n)
//// This is referred to as "excessive recursion", because it calls itself multiple times with the same parameters.
//int fibonacci(int x) {
//	if (x <= 1) {
//		return x;
//	}
//
//	return fibonacci(x - 2) + fibonacci(x - 1); // Pretty nasty when it goes past a few levels deep!
//}
//
//
//// Memoization: Storing the results of recursive functions as we go.
//// This results in a mere tiny fraction of the number of calls as fibonacci() for x that's more than a few levels deep.
//int F[50];
//int better_fib(int x) {
//
//	if (x <= 1) {
//		F[x] = x;
//		return x;
//	}
//
//	if (F[x-2] == -1) {
//		F[x-2] = better_fib(x - 2);
//	}
//	if (F[x-1] == -1) {
//		F[x-1] = better_fib(x - 1);
//	}
//	F[x] = F[x - 2] + F[x - 1];
//	return F[x];
//}
//
//// Time: O(n)
//int fib_loop(int x) {
//	int t0 = 0;
//	int t1 = 1;
//	int sum = 0;
//
//	if (x <= 1) {
//		return x;
//	}
//
//	for (int i = 2; i <= x; i++) {
//		sum = t0 + t1;
//		t0 = t1;
//		t1 = sum;
//	}
//
//	return sum;
//}
//
//int main() {
//
//	for (int i = 0; i < 50; i++) {
//		F[i] = -1;
//	}
//
//	//printf("%d , ", fibonacci(40));
//	printf("%d\n", better_fib(25));
//	printf("%d\n", fib_loop(25));
//
//	return 0;
//}
