//#include <stdio.h>
//
//typedef unsigned long long int ULLI;
//
//ULLI factorial(ULLI n) {
//	ULLI sum = 1;
//
//	for (ULLI i = 2; i <= n; i++) {
//		sum *= i;
//	}
//
//	return sum;
//}
//
//// Time: n + n + n + 1 = 3n = O(n)
//ULLI nCr(ULLI n, ULLI r) {
//	ULLI num = factorial(n);
//	ULLI den = factorial(r) * factorial(n - r);
//
//	return num / den;
//}
//
//// Different name, same functionality as above, done to reflect tutorial.
//int pascals_triangle(int n, int r) {
//	if (r == 0 || n == r) {
//		return 1;
//	}
//
//	return pascals_triangle(n - 1, r - 1) + pascals_triangle(n - 1, r);
//}
//
//int main() {
//	printf("%llu\n", nCr(15, 4));
//	printf("%d\n", pascals_triangle(15, 4));
//
//	return 0;
//}
