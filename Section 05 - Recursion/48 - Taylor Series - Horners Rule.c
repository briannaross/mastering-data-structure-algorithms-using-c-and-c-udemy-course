//#include <stdio.h>
//
//// My attempt, incorrect!
//double func(double x, double n) {
//	static double iteration = 0;
//	static double tot_top = 1;
//	static double tot_bottom = 1;
//
//	iteration++;
//
//	if (iteration > n) {
//		return 1;
//	}
//
//	tot_top *= x;
//	tot_bottom *= iteration;
//
//	return func(x, n) + (tot_top / tot_bottom);
//}
//
//double loop_e(double x, double n) {
//	double s = 1;
//
//	for (; n > 0; n--) {
//		s = 1 + (x / n) * s; // Note the formula here.
//	}
//
//	return s;
//}
//
//double recursive_e(double x, double n) {
//	static double s = 1;
//
//	if (n == 0) {
//		return s;
//	}
//	
//	s = 1 + ((x / n) * s);
//
//	return recursive_e(x, n - 1);
//}
//
//int main() {
//
//	printf("%lf\n", func(2, 10));
//
//	printf("%lf\n", loop_e(1, 10));
//	printf("%lf\n", recursive_e(1, 10));
//
//	return 0;
//}
