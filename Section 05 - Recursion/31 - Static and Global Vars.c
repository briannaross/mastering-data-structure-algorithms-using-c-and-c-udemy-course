//#include <stdio.h>
//
//// Local variable
//int func1(int n) {
//
//	if (n > 0) {
//		return func1(n - 1) + n; // Note that "+ x" is only called after the function returns!
//	}
//	return 0;
//}
//
//// Static local variable
//int func2(int n) {
//
//	static int x = 0; // 
//
//	if (n > 0) {
//		x++;
//		return func2(n - 1) + x; // Note that "+ x" is only called after the function returns!
//	}
//	return 0;
//}
//
//int main() {
//
//	int a = 5;
//
//	printf("%d", func1(a));
//	printf("\n");
//	printf("%d", func2(a));
//
//	return 0;
//}
