#include <stdio.h>

double func(double x) {
	static int iteration = 0;

	if (iteration == 0) {
		iteration++;
		return func(x) + 1;
	}

	if (iteration > x)
		return 0;

	double this_top = tot_top * x;
	double this_bottom = tot_bottom * iteration;

	return func(x, iteration + 1, tot_top, tot_bottom) + (this_top / this_bottom);
}

int main() {

	printf("%f\n", func(4));


	return 0;
}
