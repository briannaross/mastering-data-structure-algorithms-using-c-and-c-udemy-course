#include <stdio.h>

//pow(5) = 5 * 5 * 5 * 5 * 5
//       = 5 * 5 * 5 * 25
//	   = 5 * 5 * 125
//	   = 5 * 625
//	   = 3125
//
//pow (n, n - 1) = n * n * n * n * n



recursive_power(n, count) {
	if (count == 1)
		return n;

	return pow(n, count - 1) * n;
}

int main() {

	printf("%d\n", recursive_power(5, 5));

	return 0;
}