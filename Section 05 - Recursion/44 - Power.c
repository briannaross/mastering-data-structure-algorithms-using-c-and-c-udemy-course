//#include <stdio.h>
//
//int recursive_exponent(int base, int exp) {
//	if (exp == 0) {
//		return 1;
//	}
//
//	return recursive_exponent(base, exp - 1) * base;
//}
//
//int faster_rec_exp(int base, int exp) {
//	if (exp == 0) {
//		return 1;
//	}
//
//	// Split the number of multiplications in half!
//	if (exp % 2 == 0) {
//		return faster_rec_exp(base * base, exp / 2);
//	}
//
//	return (base * faster_rec_exp(base * base, (exp - 1) / 2));
//
//}
//
//int loop_exponent(int base, int exp) {
//	int s = base;
//
//	for (int i = 1; i < exp; i++) {
//		s *= base;
//	}
//
//	return s;
//}
//
//int main() {
//
//	printf("%d\n", recursive_exponent(9, 4));
//	printf("%d\n", faster_rec_exp(9, 4));
//	printf("%d\n", loop_exponent(9, 4));
//
//	return 0;
//}
