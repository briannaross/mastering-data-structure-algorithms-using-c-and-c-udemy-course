//#include <stdio.h>
//#include <stdlib.h>
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//struct Node* Create(int A[], int n) {
//	struct Node* tmp;
//	struct Node* last;
//
//	struct Node* first = (struct Node*)malloc(sizeof(struct Node));
//	if (first) {
//		first->data = A[0];
//		first->next = NULL;
//		last = first;
//
//		for (int i = 1; i < n; i++) {
//			tmp = (struct Node*)malloc(sizeof(struct Node));
//			if (tmp) {
//				tmp->data = A[i];
//				tmp->next = NULL;
//				if (last) {
//					last->next = tmp;
//					last = tmp;
//				}
//			}
//		}
//	}
//
//	return first;
//}
//
//void Display(struct Node* p) {
//	while (p != NULL) {
//		printf("%d ", p->data);
//		p = p->next;
//	}
//}
//
//int CountNodes(struct Node* p) {
//	int count = 0;
//
//	while (p != NULL) {
//		count++;
//		p = p->next;
//	}
//
//	return count;
//}
//
//int CountNodesRecursive(struct Node* p) {
//	if (p == NULL) {
//		return 0;
//	}
//
//	return 1 + CountNodesRecursive(p->next);
//}
//
//int main() {
//	struct Node* p;
//
//	int A[] = { 3, 5, 7, 10, 15 };
//	p = Create(A, 5);
//
//	Display(p);
//	printf("\n");
//
//	printf("Number of nodes (loop count): %d\n", CountNodes(p));
//	printf("Number of nodes (recursive count): %d\n", CountNodesRecursive(p));
//
//	return 0;
//}
