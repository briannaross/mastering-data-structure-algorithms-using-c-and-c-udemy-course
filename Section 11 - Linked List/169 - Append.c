//#include <stdio.h>
//#include <stdlib.h>
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//void Display(struct Node* node) {
//	while (node != NULL) {
//		printf("%d ", node->data);
//		node = node->next;
//	}
//}
//
//struct Node* Create(int A[], int n) {
//	struct Node* tmp;
//	struct Node* last;
//
//	struct Node* first = (struct Node*)malloc(sizeof(struct Node));
//	if (first) {
//		first->data = A[0];
//		first->next = NULL;
//		last = first;
//
//		for (int i = 1; i < n; i++) {
//			tmp = (struct Node*)malloc(sizeof(struct Node));
//			if (tmp) {
//				tmp->data = A[i];
//				tmp->next = NULL;
//				if (last) {
//					last->next = tmp;
//					last = tmp;
//				}
//			}
//		}
//	}
//
//	return first;
//}
//
//// I've used double pointers so that I can change the pointers to head and tail.
//void Append(struct Node **head, struct Node **tail, int data) {
//	struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
//	if (newNode) {
//		newNode->data = data;
//		newNode->next = NULL;
//
//		if (*head == NULL) {
//			(*head) = (*tail) = newNode;
//		}
//		else {
//			struct Node* curNode = (*head);
//			struct Node* prevNode = NULL;
//
//			while (curNode != NULL) {
//				prevNode = curNode;
//				curNode = curNode->next;
//			}
//
//			prevNode->next = newNode;
//		}
//	}
//}
//
//int main() {
//	struct Node* pHead = NULL;
//	struct Node* pTail = NULL;
//
//	int A[] = { 3, 5, 7, 10, 25, 8, 12, 2 };
//	pHead = Create(A, 8);
//
//	int data = 0;
//
//	while (data != -1) {
//		printf("Enter data for new node (range 1..2147483647), -1 to quit: ");
//		scanf_s("%d", &data);
//
//		if (data >= 0) {
//			Append(&pHead, &pTail, data);
//			Display(pHead);
//			printf("\n");
//		}
//		else if (data < -1) {
//			printf("Invalid data.\n");
//		}
//	}
//
//	return 0;
//}
