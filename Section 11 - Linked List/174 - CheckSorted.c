//#include <stdio.h>
//#include <stdlib.h>
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//struct Node* Create(int A[], int n) {
//	struct Node* newNode;
//	struct Node* last;
//
//	struct Node* first = (struct Node*)malloc(sizeof(struct Node));
//	if (first) {
//		first->data = A[0];
//		first->next = NULL;
//		last = first;
//
//		for (int i = 1; i < n; i++) {
//			newNode = (struct Node*)malloc(sizeof(struct Node));
//			if (newNode) {
//				newNode->data = A[i];
//				newNode->next = NULL;
//				if (last) {
//					last->next = newNode;
//					last = newNode;
//				}
//			}
//		}
//	}
//
//	return first;
//}
//
//void Display(struct Node* node) {
//	while (node != NULL) {
//		printf("%d ", node->data);
//		node = node->next;
//	}
//}
//
//int CountNodes(struct Node* node) {
//	int count = 0;
//
//	while (node != NULL) {
//		count++;
//		node = node->next;
//	}
//
//	return count;
//}
//
//// I've used double pointers so that I can change the pointers to head and tail.
//int IsSorted(struct Node* node) {
//	int lastVal = -32768;
//	while (node != NULL) {
//		if (node->data < lastVal) {
//			return 0;
//		} else {
//			lastVal = node->data;
//			node = node->next;
//		}
//	}
//
//	return 1;
//}
//
//int main() {
//	struct Node* node = NULL;
//
//	int A[] = { 2, 3, 5, 7, 8, 10, 12, 25 };
//	node = Create(A, 8);
//
//	int pos = 0;
//	int result = 0;
//
//	result = IsSorted(node);
//
//	if (result == 1) {
//		printf("List is sorted.\n");
//	} else {
//		printf("List is not sorted.\n");
//	}
//
//	return 0;
//}
