#include <stdio.h>
#include <stdlib.h>

struct Node {
	int data;
	struct Node* next;
};

struct Node* Create(int A[], int n) {
	struct Node* tmp;
	struct Node* last;

	struct Node* first = (struct Node*)malloc(sizeof(struct Node));
	if (first) {
		first->data = A[0];
		first->next = NULL;
		last = first;

		for (int i = 1; i < n; i++) {
			tmp = (struct Node*)malloc(sizeof(struct Node));
			if (tmp) {
				tmp->data = A[i];
				tmp->next = NULL;
				if (last) {
					last->next = tmp;
					last = tmp;
				}
			}
		}
	}

	return first;
}

void Display(struct Node* p) {
	while (p != NULL) {
		printf("%d ", p->data);
		p = p->next;
	}
}

int SumNodes(struct Node* p) {
	int sum = 0;

	while (p != NULL) {
		sum += p->data;
		p = p->next;
	}

	return sum;
}

int SumNodesRecursive(struct Node* p) {
	if (p == NULL) {
		return 0;
	}

	return p->data + SumNodesRecursive(p->next);
}

int main() {
	struct Node* p;

	int A[] = { 3, 5, 7, 10, 15 };
	p = Create(A, 5);

	Display(p);
	printf("\n");

	printf("Sum of nodes (loop count): %d\n", SumNodes(p));
	printf("Sum of nodes (recursive count): %d\n", SumNodesRecursive(p));

	return 0;
}
