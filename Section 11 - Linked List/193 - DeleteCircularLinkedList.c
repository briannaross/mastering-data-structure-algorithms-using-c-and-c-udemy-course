//#include <stdio.h>
//#include <stdlib.h>
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//// Create a circular linked list
//struct Node* Create(int A[], int n) {
//	int i;
//	struct Node* head = (struct Node*)malloc(sizeof(struct Node));
//
//	if (head) {
//		head->data = A[0];
//		head->next = head;
//		struct Node* last = head;
//		if (last) {
//			for (i = 1; i < n; i++) {
//				struct Node* curNd = (struct Node*)malloc(sizeof(struct Node));
//				if (curNd) {
//					curNd->data = A[i];
//					curNd->next = last->next;
//					last->next = curNd;
//					last = curNd;
//				}
//			}
//		}
//	}
//	return head;
//}
//
//// Get the length of a circular linked list
//int Length(struct Node* head) {
//	struct Node* curNd = head;
//	int len = 0;
//
//	do {
//		len++;
//		curNd = curNd->next;
//	} while (curNd != head);
//
//	return len;
//}
//
//// Display all nodes of a circular linked list
//void Display(struct Node* head) {
//	struct Node* curNd = head;
//	do {
//		printf("%d ", curNd->data);
//		curNd = curNd->next;
//	} while (curNd != head);
//	printf("\n");
//}
//
//// Delete node from a circular linked list, returning it's data
//int Delete(struct Node** head, int index) {
//	struct Node* curNd = *head;
//	struct Node* delNd;
//	int delNdData; // Deleted nodes' data
//
//	// Return if index is out of bounds
//	if (index < 0 || index > Length(*head)) {
//		return -1;
//	}
//
//	// Delete head node
//	if (index == 1) {
//		while (curNd->next != *head) {
//			curNd = curNd->next;
//		}
//		delNdData = (*head)->data;
//		// Head is only node
//		if (*head == curNd) {
//			free(*head);
//			*head = NULL;
//		}
//		// Linked list contains other nodes
//		else {
//			curNd->next = (*head)->next;
//			free(*head);
//			*head = curNd->next;
//		}
//	}
//	// Delete node at a given position
//	else {
//		for (int i = 0; i < index - 2; i++) {
//			curNd = curNd->next;
//		}
//		delNd = curNd->next;
//		curNd->next = delNd->next;
//		delNdData = delNd->data;
//		free(delNd);
//	}
//
//	return delNdData;
//
//}
//
//int main() {
//	int A[] = { 2, 3, 4, 5, 6 };
//
//	struct Node* head = Create(A, 5);
//
//	Delete(&head, 8);
//
//	Display(head, head);
//
//	return 0;
//}