//#include <stdio.h>
//#include <stdlib.h>
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//struct Node* Create(int A[], int n) {
//	struct Node* newNode;
//	struct Node* last;
//
//	struct Node* first = (struct Node*)malloc(sizeof(struct Node));
//	if (first) {
//		first->data = A[0];
//		first->next = NULL;
//		last = first;
//
//		for (int i = 1; i < n; i++) {
//			newNode = (struct Node*)malloc(sizeof(struct Node));
//			if (newNode) {
//				newNode->data = A[i];
//				newNode->next = NULL;
//				if (last) {
//					last->next = newNode;
//					last = newNode;
//				}
//			}
//		}
//	}
//
//	return first;
//}
//
//void Display(struct Node* node) {
//	while (node != NULL) {
//		printf("%d ", node->data);
//		node = node->next;
//	}
//	printf("\n");
//}
//
//int CountNodes(struct Node* node) {
//	int count = 0;
//
//	while (node != NULL) {
//		count++;
//		node = node->next;
//	}
//
//	return count;
//}
//
//// Only works on a sorted list
//void Reverse(struct Node* head) {
//	if (head) {
//		int* arr = malloc(sizeof(int) * CountNodes(head));
//		struct Node* node = head;
//
//		if (arr && node) {
//			int i = 0;
//
//			while (node != NULL) {
//				arr[i++] = node->data;
//				node = node->next;
//			}
//
//			node = head;
//			i--;
//
//			while (node != NULL) {
//				node->data = arr[i--];
//				node = node->next;
//			}
//		}
//
//		free(arr);
//	}
//}
//
//int main() {
//	struct Node* head = NULL;
//
//	int A[] = { 1, 2, 3, 4, 5 };
//	head = Create(A, 5);
//
//	Display(head);
//
//	Reverse(head);
//
//	Display(head);
//
//	return 0;
//}
