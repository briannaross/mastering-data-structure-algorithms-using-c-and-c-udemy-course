//#include <stdio.h>
//#include <stdlib.h>
//
//#define MIN_INT -32768
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//struct Node* Create(int A[], int n) {
//	struct Node* tmp;
//	struct Node* last;
//
//	struct Node* first = (struct Node*)malloc(sizeof(struct Node));
//	if (first) {
//		first->data = A[0];
//		first->next = NULL;
//		last = first;
//
//		for (int i = 1; i < n; i++) {
//			tmp = (struct Node*)malloc(sizeof(struct Node));
//			if (tmp) {
//				tmp->data = A[i];
//				tmp->next = NULL;
//				if (last) {
//					last->next = tmp;
//					last = tmp;
//				}
//			}
//		}
//	}
//
//	return first;
//}
//
//void Display(struct Node* p) {
//	while (p != NULL) {
//		printf("%d ", p->data);
//		p = p->next;
//	}
//}
//
//struct Node* ImprovedSearch(struct Node* p, int key) {
//	if (key == p->data) {
//		return p;
//	}
//
//	struct Node* head = p;
//	struct Node* q = p;
//	p = p->next;
//
//	while (p != NULL) {
//		if (key == p->data) {
//			q->next = p->next;	// Set following pointer q to point to the node after p
//			p->next = head;		// Head becomes the second node by pointing p->next to it
//			head = p;			// Make p the head node
//			return head;
//		}
//		q = p;
//		p = p->next;
//	}
//
//	return NULL;
//}
//
//int main() {
//	struct Node* p;
//
//	int A[] = { 3, 5, 7, 10, 25, 8, 12, 2 };
//	p = Create(A, 8);
//
//	if (p)
//	{
//		Display(p);
//		printf("\n");
//
//		int key;
//		printf("Enter a value to search for: ");
//		scanf_s("%d", &key);
//
//		p = ImprovedSearch(p, key);
//		if (p) {
//			printf("Key found: %d\n", p->data);
//		}
//		else {
//			printf("Key not found :-(\n");
//		}
//
//		Display(p);
//		printf("\n");
//	}
//
//	return 0;
//}
