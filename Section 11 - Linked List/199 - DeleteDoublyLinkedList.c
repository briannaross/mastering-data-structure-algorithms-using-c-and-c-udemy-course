#include <stdio.h>
#include <stdlib.h>

struct Node {
	struct Node* prev;
	int data;
	struct Node* next;
};

void Display(struct Node* head) {
	struct Node* curNd = head;

	while (curNd) {
		printf("%d ", curNd->data);
		curNd = curNd->next;
	}
	printf("\n");
}

int Length(struct Node* head) {
	struct Node* curNd = head;
	int len = 0;

	while (curNd) {
		len++;
		curNd = curNd->next;
	}

	return len;
}

struct Node* Create(int A[], int n) {
	struct Node* tail;
	int i;

	struct Node* head = (struct Node*)malloc(sizeof(struct Node));
	if (head) {
		head->data = A[0];
		head->prev = head->next = NULL;
		tail = head;

		for (i = 1; i < n; i++) {
			struct Node* curNd = (struct Node*)malloc(sizeof(struct Node));
			if (curNd) {
				curNd->data = A[i];
				curNd->next = tail->next;
				curNd->prev = tail;
				tail->next = curNd;
				tail = curNd;
			}
		}
	}

	return head;
}

struct Node* Insert(struct Node* head, int index, int x) {
	if (index < 0 || index > Length(head)) {
		return head;
	}

	struct Node* newNd = (struct Node*)malloc(sizeof(struct Node));
	if (newNd) {
		newNd->data = x;

		// Insert node at start of DLL
		if (index == 0) {
			newNd->prev = NULL;
			newNd->next = head;
			head->prev = newNd;
			head = newNd;
		}
		// Insert node at given index of DLL
		else {
			struct Node* curNd = head;
			for (int i = 0; i < index - 1; i++) {
				curNd = curNd->next;
			}
			newNd->prev = curNd;
			newNd->next = curNd->next;
			if (curNd->next) {
				curNd->next->prev = newNd;
			}

			if (curNd) {
				curNd->next = newNd;
			}
		}
	}

	return head;
}

struct Node* Delete(struct Node* head, int index) {
	if (index < 1 || index > Length(head)) {
		return head;
	}

	int data = 0;

	struct Node *curNd = head;
	if (curNd) {
		if (index == 1) {
			head = head->next;
			if (head) {
				head->prev = NULL;
			}
		}
		else {
			for (int i = 0; i < index - 1; i++) {
				curNd = curNd->next;
			}
			curNd->prev->next = curNd->next;
			if (curNd->next) {
				curNd->next->prev = curNd->prev;
			}
		}

		if (curNd) {
			data = curNd->data;
			free(curNd);
		}
	}

	return head;
}

int main() {
	int A[] = { 2, 3, 4, 5, 6 };

	struct Node* head = Create(A, 5);

	printf("\nLength is: %d\n", Length(head));

	Display(head);

	head = Insert(head, 0, 25);

	Display(head);

	head = Delete(head, 5);
	Display(head);
	head = Delete(head, 2);
	Display(head);
	head = Delete(head, 1);
	Display(head);

	return 0;
}
