//#include <stdio.h>
//#include <stdlib.h>
//
//#define MIN_INT -32768
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//struct Node* Create(int A[], int n) {
//	struct Node* tmp;
//	struct Node* last;
//
//	struct Node* first = (struct Node*)malloc(sizeof(struct Node));
//	if (first) {
//		first->data = A[0];
//		first->next = NULL;
//		last = first;
//
//		for (int i = 1; i < n; i++) {
//			tmp = (struct Node*)malloc(sizeof(struct Node));
//			if (tmp) {
//				tmp->data = A[i];
//				tmp->next = NULL;
//				if (last) {
//					last->next = tmp;
//					last = tmp;
//				}
//			}
//		}
//	}
//
//	return first;
//}
//
//void Display(struct Node* p) {
//	while (p != NULL) {
//		printf("%d ", p->data);
//		p = p->next;
//	}
//}
//
//int MaxValue(struct Node* p) {
//	int max = p->data;
//
//	while (p != NULL) {
//		if (p->data > max) {
//			max = p->data;
//		}
//		p = p->next;
//	}
//
//	return max;
//}
//
//int MaxValueRecursive(struct Node* p) {
//	if (p == NULL) {
//		return MIN_INT;
//	}
//
//	int x = MaxValueRecursive(p->next);
//
//	return (x > p->data) ? x : p->data; // Ternary operator
//}
//
//int main() {
//	struct Node* p;
//
//	int A[] = { 3, 5, 7, 10, 25, 8, 12, 2 };
//	p = Create(A, 8);
//
//	if (p)
//	{
//		Display(p);
//		printf("\n");
//
//		printf("Max value of nodes (loop count): %d\n", MaxValue(p));
//		printf("Max value of nodes (recursive count): %d\n", MaxValueRecursive(p));
//	}
//
//	return 0;
//}
