//#include <stdio.h>
//#include <stdlib.h>
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//struct Node* Create(int A[], int n) {
//	struct Node* tmp;
//	struct Node* last;
//
//	struct Node* first = (struct Node*)malloc(sizeof(struct Node));
//	if (first) {
//		first->data = A[0];
//		first->next = NULL;
//		last = first;
//
//		for (int i = 1; i < n; i++) {
//			tmp = (struct Node*)malloc(sizeof(struct Node));
//			if (tmp) {
//				tmp->data = A[i];
//				tmp->next = NULL;
//				if (last) {
//					last->next = tmp;
//					last = tmp;
//				}
//			}
//		}
//	}
//
//	return first;
//}
//
//void Display(struct Node* node) {
//	while (node != NULL) {
//		printf("%d ", node->data);
//		node = node->next;
//	}
//}
//
//int CountNodes(struct Node* p) {
//	int count = 0;
//
//	while (p != NULL) {
//		count++;
//		p = p->next;
//	}
//
//	return count;
//}
//
//// I've used double pointers so that I can change the pointers to head and tail.
//int Delete(struct Node** head, int pos) {
//	if (pos < 1 || pos > CountNodes(*head)) {
//		return 0;
//	}
//
//	if (*head) {
//		struct Node* curNode = (*head);
//
//		if (pos == 1) {
//			(*head) = (*head)->next;
//			free(curNode);
//		}
//		else {
//			struct Node* prevNode = (*head);
//			curNode = (*head)->next;
//
//			for (int i = 1; i < pos - 1; i++) {
//				prevNode = curNode;
//				curNode = curNode->next;
//			}
//
//			prevNode->next = curNode->next;
//			free(curNode);
//		}
//	}
//
//	return 1;
//}
//
//int main() {
//	struct Node* p = NULL;
//
//	int A[] = { 3, 5, 7, 10, 25, 8, 12, 2 };
//	p = Create(A, 8);
//
//	int pos = 0;
//
//	while (pos != -1) {
//		printf("Enter position of node to be deleted or -1 to quit: ");
//		scanf_s("%d", &pos);
//
//		if (Delete(&p, pos)) {
//			Display(p);
//			printf("\n");
//		}
//		else {
//			printf("Position out of range.\n");
//		}
//	}
//
//	return 0;
//}
