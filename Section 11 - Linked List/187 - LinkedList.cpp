//#include <iostream>
//
//using namespace std;
//
//// TODO: Could convert this to a templated class
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//class LinkedList {
//private:
//	struct Node* head;
//	struct Node* tail;
//	int numNodes;
//public:
//	LinkedList();
//	LinkedList(int [], int);
//	void Display();
//	int CountNodes();
//	int SumNodes();
//	int MaxValue();
//	struct Node* ImprovedSearch(int);
//	void Insert(int, int);
//	void Append(int);
//	void InsertSorted(int);
//	int Delete(int);
//	bool IsSorted();
//	void RemoveDuplicates();
//	void Reverse();
//	void Concat(struct Node*);
//	void Merge(struct Node*);
//	bool IsLoop();
//};
//
//LinkedList::LinkedList() {
//	head = NULL;
//	tail = NULL;
//	numNodes = 0;
//}
//
//LinkedList::LinkedList(int A[], int n) {
//	struct Node* node;
//
//	head = new Node();
//	tail = head;
//	numNodes = 0;
//
//	if (head) {
//		head->data = A[0];
//		head->next = NULL;
//
//		for (int i = 1; i < n; i++) {
//			node = new Node();
//			if (node) {
//				node->data = A[i];
//				node->next = NULL;
//				if (tail) {
//					tail->next = node;
//					tail = node;
//				}
//			}
//		}
//	}
//
//	numNodes = CountNodes();
//}
//
//// Lesson 154: Display all nodes
//void LinkedList::Display() {
//	struct Node* node = head;
//	while (node != NULL) {
//		cout << node->data << " ";
//		node = node->next;
//	}
//	cout << endl;
//}
//
//// Lesson 158: Return count of the number of nodes
//// Redundant if maintaining a counter of nodes, but leaving in as it's part of the tutorial series
//int LinkedList::CountNodes() {
//	struct Node* node = head;
//	int count = 0;
//
//	while (node != NULL) {
//		count++;
//		node = node->next;
//	}
//
//	return count;
//}
//
//// Lesson 159: Return sum of all nodes
//int LinkedList::SumNodes() {
//	struct Node* node = head;
//	int sum = 0;
//
//	while (node != NULL) {
//		sum += node->data;
//		node = node->next;
//	}
//
//	return sum;
//}
//
//// Lesson 161: Return maximum value
//int LinkedList::MaxValue() {
//	struct Node* node = head;
//	int max = head->data;
//
//	while (node != NULL) {
//		if (node->data > max) {
//			max = node->data;
//		}
//		node = node->next;
//	}
//
//	return max;
//}
//
//// Lesson 164: Search for a value using a key
//struct Node* LinkedList::ImprovedSearch(int key) {
//	if (key == head->data) {
//		return head;
//	}
//
//	struct Node* node = head;
//	struct Node* q = node;
//	node = node->next;
//
//	while (node != NULL) {
//
//		// TODO: Update classes' tail pointer
//		if (key == node->data) {
//			q->next = node->next;	// Set following pointer q to point to the node after p
//			node->next = head;		// Head becomes the second node by pointing p->next to it
//			head = node;			// Make p the head node
//			return head;
//		}
//		q = node;
//		node = node->next;
//	}
//
//	return NULL;
//}
//
//// Lesson 165: Insert a new node
//void LinkedList::Insert(int data, int pos) {
//	if (pos < 0 || pos > CountNodes()) {
//		return;
//	}
//
//	struct Node* node = head;
//	struct Node* t = new Node();
//	t->data = data;
//
//	if (pos == 0) {
//		t->next = head;
//		head = t;
//	}
//
//	// TODO: Update classes' tail pointer
//	else if (pos > 0) {
//		for (int i = 0; i < pos - 1; i++) {
//			node = node->next;
//		}
//		if (node) {
//			t->next = node->next;
//			node->next = t;
//		}
//	}
//
//	numNodes = CountNodes();
//}
//
//// Lesson 169: Append node to end of list
//void LinkedList::Append(int data) {
//	struct Node* newNode = new Node();
//	newNode->data = data;
//	newNode->next = NULL;
//
//	if (head == NULL) {
//		head = tail = newNode;
//	}
//	else {
//		struct Node* node = head;
//		struct Node* prevNode = NULL;
//
//		while (node != NULL) {
//			prevNode = node;
//			node = node->next;
//		}
//
//		prevNode->next = newNode;
//		tail = newNode;
//	}
//
//	numNodes = CountNodes();
//}
//
//// Lesson 170: Insert a node into a sorted list maintaining sort order
//void LinkedList::InsertSorted(int data) {
//	struct Node* node = head;
//	struct Node* newNode = new Node();
//	newNode->data = data;
//	newNode->next = NULL;
//
//	if (head == NULL) {
//		head = tail = newNode;	// Set head and tail to the new node because the list is empty here.
//	}
//	else {
//		// TODO: Update classes' tail pointer
//		if (node->data > data) {
//			newNode->next = node;
//			head = newNode;
//		}
//		else {
//			struct Node* prevNode = node;
//			node = node->next;
//			while (node && node->data < data) {
//				prevNode = node;
//				node = node->next;
//			}
//
//			newNode->next = node;
//			prevNode->next = newNode;
//		}
//	}
//
//	numNodes = CountNodes();
//}
//
//// Lesson 172: Delete a node at a given position and return its data
//int LinkedList::Delete(int pos) {
//	if (pos < 1 || pos > CountNodes()) {
//		return 0;
//	}
//
//	struct Node* node = head;
//	int data = 0;
//
//	// TODO: Update classes' tail pointer
//	if (pos == 1) {
//		head = head->next;
//		data = node->data;
//		delete node;
//	}
//	else {
//		struct Node* prevNode = node;
//		node = node->next;
//
//		for (int i = 1; i < pos - 1; i++) {
//			prevNode = node;
//			node = node->next;
//		}
//
//		prevNode->next = node->next;
//		data = node->data;
//		delete node;
//	}
//
//	numNodes = CountNodes();
//	return data;
//}
//
//// Lesson 174: Check is list is sorted
//bool LinkedList::IsSorted() {
//	struct Node* node = head;
//	int lastVal = -32768;
//
//	while (node != NULL) {
//		if (node->data < lastVal) {
//			return false;
//		} else {
//			lastVal = node->data;
//			node = node->next;
//		}
//	}
//
//	return true;
//}
//
//// Lesson 176: Remove duplicates from a sorted list
//void LinkedList::RemoveDuplicates() {
//	struct Node* node = head;
//	struct Node* q = node->next;
//
//	// TODO: Update classes' tail pointer
//	while (q != NULL) {
//		if (node->data == q->data) {
//			node->next = q->next;
//			delete q;
//			q = node->next;
//		}
//		else {
//			node = q;
//			q = q->next;
//		}
//	}
//
//	numNodes = CountNodes();
//}
//
//// Lesson 178: Reverse the list
//void LinkedList::Reverse() {
//	struct Node* node = head;
//	struct Node* q = NULL;
//	struct Node* r = NULL;
//
//	while (node != NULL) {
//		r = q;
//		q = node;
//		node = node->next;
//		q->next = r;
//	}
//	head = q;
//}
//
//// Lesson 181: Concatentate two lists, tacking list2 onto the end of list1
//void LinkedList::Concat(struct Node* head2) {
//	struct Node* node = head;
//
//	// TODO: Update classes' tail pointer
//	while (node->next != NULL) {
//		node = node->next;
//	}
//
//	node->next = head2;
//
//	numNodes = CountNodes();
//}
//
//// Lesson 183: Merge list with another list
//void LinkedList::Merge(struct Node* q) {
//	struct Node* node = head;
//	struct Node* last = NULL;
//
//	// TODO: Update classes' tail pointer
//	if (node && q) {
//		if (node->data < q->data) {
//			last = node;
//			node = node->next;
//		}
//		else {
//			last = q;
//			q = q->next;
//		}
//		head = last;	// Set new head of list, dependent on which first node has the lowest value.
//
//		while (node && q) {
//			if (node->data < q->data) {
//				last->next = node;
//				last = node;
//				node = node->next;
//			}
//			else {
//				last->next = q;
//				last = q;
//				q = q->next;
//			}
//		}
//
//		if (node) {
//			last->next = node;
//		}
//		else {
//			last->next = q;
//		}
//	}
//
//	numNodes = CountNodes();
//}
//
//// Lesson 185: Check if list contains a loop
//bool LinkedList::IsLoop() {
//	struct Node* node = NULL;
//	struct Node* q = NULL;
//
//	node = q = head;
//
//	do {
//		node = node->next;
//		q = q->next;
//		q = q != NULL ? q->next : q;
//	} while (node && q && node != q);
//
//	return node == q ? true : false;
//}
//
//int main() {
//	// TODO: Could write a menu system here
//
//	int A[] = { 1, 3, 3, 7, 9, 11 };
//	LinkedList list = LinkedList(A, 6);
//
//	// Display Linked List
//	list.Display();
//
//	// Display count of number of nodes
//	cout << list.CountNodes() << endl << endl;
//
//	// Display sum of all nodes
//	cout << list.SumNodes() << endl << endl;
//
//	// Display maximum value
//	cout << list.MaxValue() << endl << endl;
//
//	// Display data of key from key search
//	struct Node* tmp = list.ImprovedSearch(1);
//	if (tmp) {
//		cout << tmp->data << endl << endl;
//	}
//
//	// Insert a new node at a given position, then display list
//	list.Insert(55, 6);
//	list.Display();
//	cout << endl;
//
//	// Append a new node to the list, then display list
//	list.Append(85);
//	list.Display();
//	cout << endl;
//
//	// Insert a new node whilst maintain a sorted list, then display list
//	list.InsertSorted(-1);
//	list.Display();
//	cout << endl;
//
//	// Delete node at a given position, displaying its data
//	cout << list.Delete(9) << endl;
//	list.Display();
//	cout << endl;
//
//	// Check if list is sorted
//	list.IsSorted() ? cout << "List is sorted" : cout << "List is not sorted";
//	cout << endl << endl;
//
//	// Remove duplicates from the sorted list, then display list
//	list.RemoveDuplicates();
//	list.Display();
//	cout << endl;
//
//	// Reverse the list, then display, then reverse it back to keep it ordered
//	list.Reverse();
//	list.Display();
//	list.Reverse();
//	list.Display();
//	cout << endl;
//
//	// Concatenate a new list onto the existing one, then display
//	struct Node* concat = new Node();
//	concat->data = 66;
//	concat->next = new Node();
//	concat->next->data = 77;
//	concat->next->next = new Node();
//	concat->next->next->data = 88;
//	concat->next->next->next = NULL;
//	list.Concat(concat);
//	list.Display();
//	cout << endl;
//
//	struct Node* merge = new Node();
//	merge->data = -2;
//	merge->next = new Node();
//	merge->next->data = 45;
//	merge->next->next = new Node();
//	merge->next->next->data = 12345;
//	merge->next->next->next = NULL;
//	list.Merge(merge);
//	list.Display();
//	cout << endl;
//
//	// Check for loop in list
//	// NOTE: Don't call any other functions while using a LOOPed list as they may execute forever!
//	//list.head->next->next->next->next->next = list.head;
//	list.IsLoop() ? cout << "List contains a loop" : cout << "List does not contain a loop";
//	cout << endl;
//
//	return 0;
//}
