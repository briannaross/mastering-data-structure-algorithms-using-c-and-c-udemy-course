//#include <stdio.h>
//#include <stdlib.h>
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//// Create a circular linked list
//struct Node* Create(int A[], int n) {
//	int i;
//	struct Node* head = (struct Node*)malloc(sizeof(struct Node));
//
//	if (head) {
//		head->data = A[0];
//		head->next = head;
//		struct Node* last = head;
//		if (last) {
//			for (i = 1; i < n; i++) {
//				struct Node* curNd = (struct Node*)malloc(sizeof(struct Node));
//				if (curNd) {
//					curNd->data = A[i];
//					curNd->next = last->next;
//					last->next = curNd;
//					last = curNd;
//				}
//			}
//		}
//	}
//	return head;
//}
//
//// Get the length of a circular linked list
//int Length(struct Node* head) {
//	struct Node* curNd = head;
//	int len = 0;
//
//	do {
//		len++;
//		curNd = curNd->next;
//	} while (curNd != head);
//
//	return len;
//}
//
//// Display all nodes of a circular linked list
//void Display(struct Node* head) {
//	struct Node* curNd = head;
//	do {
//		printf("%d ", curNd->data);
//		curNd = curNd->next;
//	} while (curNd != head);
//	printf("\n");
//}
//
//// Insert node into a circular linked list
//void Insert(struct Node** head, int index, int x) {
//	struct Node* newNd;
//	struct Node* curNd = *head;
//	int i;
//
//	// Return if index is out of bounds
//	if (index < 0 || index > Length(*head)) {
//		return;
//	}
//
//	// Node to be inserted before head
//	if (index == 0) {
//		newNd = (struct Node*)malloc(sizeof(struct Node));
//		if (newNd) {
//			newNd->data = x;
//			// Linked list is current empty
//			if (*head == NULL) {
//				*head = newNd;
//				(*head)->next = head;
//			}
//			// Linked list contains nodes
//			else {
//				// Find node just before head
//				while (curNd->next != *head) {
//					curNd = curNd->next;
//				}
//				curNd->next = newNd;
//				newNd->next = *head;
//				*head = newNd;
//			}
//		}
//	}
//	// Node to be inserted at a given index
//	else {
//		// Find node just before node at index
//		for (i = 0; i < index - 1; i++) {
//			curNd = curNd->next;
//		}
//		newNd = (struct Node*)malloc(sizeof(struct Node));
//		if (newNd) {
//			newNd->data = x;
//			newNd->next = curNd->next;
//			curNd->next = newNd;
//		}
//
//	}
//}
//
//int main() {
//	int A[] = { 2, 3, 4, 5, 6 };
//
//	struct Node* head = Create(A, 5);
//
//	Insert(&head, 0, 10);
//
//	Display(head, head);
//
//	return 0;
//}