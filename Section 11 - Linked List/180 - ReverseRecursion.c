//#include <stdio.h>
//#include <stdlib.h>
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//struct Node* Create(int A[], int n) {
//	struct Node* newNode;
//	struct Node* last;
//
//	struct Node* first = (struct Node*)malloc(sizeof(struct Node));
//	if (first) {
//		first->data = A[0];
//		first->next = NULL;
//		last = first;
//
//		for (int i = 1; i < n; i++) {
//			newNode = (struct Node*)malloc(sizeof(struct Node));
//			if (newNode) {
//				newNode->data = A[i];
//				newNode->next = NULL;
//				if (last) {
//					last->next = newNode;
//					last = newNode;
//				}
//			}
//		}
//	}
//
//	return first;
//}
//
//void Display(struct Node* node) {
//	while (node != NULL) {
//		printf("%d ", node->data);
//		node = node->next;
//	}
//	printf("\n");
//}
//
//int CountNodes(struct Node* node) {
//	int count = 0;
//
//	while (node != NULL) {
//		count++;
//		node = node->next;
//	}
//
//	return count;
//}
//
//// Reverse the list using recursion.
//void ReverseR(struct Node** head, struct Node* q, struct Node* p) {
//	if (p) {
//		ReverseR(head, p, p->next);
//		p->next = q;
//	}
//	else {
//		*(head) = q;
//	}
//}
//
//int main() {
//	int A[] = { 72, 97, 1657, 328, 4 };
//	struct Node* head = Create(A, 5);
//
//	Display(head);
//
//	ReverseR(&head, NULL, head);
//
//	Display(head);
//
//	return 0;
//}
