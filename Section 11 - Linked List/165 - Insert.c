//#include <stdio.h>
//#include <stdlib.h>
//
//#define MIN_INT -32768
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//struct Node* Create(int A[], int n) {
//	struct Node* tmp;
//	struct Node* last;
//
//	struct Node* first = (struct Node*)malloc(sizeof(struct Node));
//	if (first) {
//		first->data = A[0];
//		first->next = NULL;
//		last = first;
//
//		for (int i = 1; i < n; i++) {
//			tmp = (struct Node*)malloc(sizeof(struct Node));
//			if (tmp) {
//				tmp->data = A[i];
//				tmp->next = NULL;
//				if (last) {
//					last->next = tmp;
//					last = tmp;
//				}
//			}
//		}
//	}
//
//	return first;
//}
//
//void Display(struct Node* p) {
//	while (p != NULL) {
//		printf("%d ", p->data);
//		p = p->next;
//	}
//}
//
//int Count(struct Node* p) {
//	int count = 0;
//
//	while (p != NULL) {
//		count++;
//		p = p->next;
//	}
//
//	return count;
//}
//
//struct Node* Insert(struct Node* head, int data, int pos) {
//	if (pos < 0 || pos > Count(head)) {
//		return head;
//	}
//
//	struct Node* p = head;
//	struct Node* t = (struct Node*)malloc(sizeof(struct Node));
//
//	if (t) {
//		t->data = data;
//
//		if (pos == 0) {
//			t->next = head;
//			head = t;
//		}
//		else if (pos > 0) {
//			for (int i = 0; i < pos - 1; i++) {
//				p = p->next;
//			}
//			if (p) {
//				t->next = p->next;
//				p->next = t;
//			}
//		}
//	}
//
//	return head;
//}
//
//int main() {
//	struct Node* p;
//
//	int A[] = { 3, 5, 7, 10, 25, 8, 12, 2 };
//	p = Create(A, 8);
//
//	if (p)
//	{
//		Display(p);
//		printf("\n");
//
//		int data;
//		int pos;
//		printf("Enter data and a position to insert at: ");
//		scanf_s("%d%d", &data, &pos);
//
//		p = Insert(p, data, pos);
//
//		Display(p);
//		printf("\n");
//	}
//
//	return 0;
//}
