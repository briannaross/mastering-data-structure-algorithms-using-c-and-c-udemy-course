//#include <stdio.h>
//#include <stdlib.h>
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//void Display(struct Node* p) {
//	while (p != NULL) {
//		printf("%d ", p->data);
//		p = p->next;
//	}
//}
//
//void InsertSorted(struct Node** head, int data) {
//	struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
//
//	if (newNode) {
//		newNode->data = data;
//		newNode->next = NULL;
//
//		if ((*head) == NULL) {
//			(*head) = newNode;	// Set head and tail to the new node because the list is empty here.
//		}
//		else {
//			if ((*head)->data > data) {
//				newNode->next = (*head);
//				(*head) = newNode;
//			}
//			else {
//				struct Node* prevNode = (*head);
//				struct Node* curNode = (*head)->next;
//				while (curNode && curNode->data < data) {
//					prevNode = curNode;
//					curNode = curNode->next;
//				}
//
//				newNode->next = curNode;
//				prevNode->next = newNode;
//			}
//		}
//	}
//}
//
//int main() {
//	struct Node* p = NULL;
//
//	int data = 0;
//	while (data != -1) {
//		printf("Enter data for next node (range 1..2147483647) or -1 to quit: ");
//		scanf_s("%d", &data);
//	
//		if (data >= 0) {
//			InsertSorted(&p, data);
//			Display(p);
//			printf("\n");
//		}
//		else if (data < -1) {
//			printf("Invalid data.\n");
//		}
//	}
//
//	return 0;
//}
