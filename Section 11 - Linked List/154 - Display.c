//#include <stdio.h>
//#include <stdlib.h>
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//struct Node* Create(int A[], int n) {
//	struct Node* tmp;
//	struct Node *last;
//
//	struct Node* first = (struct Node*)malloc(sizeof(struct Node));
//	if (first) {
//		first->data = A[0];
//		first->next = NULL;
//		last = first;
//
//		for (int i = 1; i < n; i++) {
//			tmp = (struct Node*)malloc(sizeof(struct Node));
//			if (tmp) {
//				tmp->data = A[i];
//				tmp->next = NULL;
//				if (last) {
//					last->next = tmp;
//					last = tmp;
//				}
//			}
//		}
//	}
//
//	return first;
//}
//
//void Display(struct Node* p) {
//	while (p != NULL) {
//		printf("%d ", p->data);
//		p = p->next;
//	}
//
//	// Can use a for loop instead
//	//for (struct Node* i = p; i != NULL; i = i->next) {
//	//	printf("%d ", i->data);
//	//}
//}
//
//void DisplayRecursive(struct Node* p) {
//	// Display list in a forward order
//	if (p != NULL) {
//		printf("%d ", p->data);
//		DisplayRecursive(p->next);
//	}
//}
//
//// Display list in reverse order
//void DisplayRecursiveReverse(struct Node* p) {
//	if (p != NULL) {
//		DisplayRecursiveReverse(p->next);
//		printf("%d ", p->data);
//	}
//}
//
//int main() {
//	struct Node* p;
//
//	int A[] = { 3, 5, 7, 10, 15 };
//	p = Create(A, 5);
//
//	Display(p);
//	printf("\n");
//	DisplayRecursive(p);
//	printf("\n");
//	DisplayRecursiveReverse(p);
//	printf("\n");
//
//	return 0;
//}
