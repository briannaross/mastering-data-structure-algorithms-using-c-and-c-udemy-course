//#include <stdio.h>
//#include <stdlib.h>
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//struct Node* Create(int A[], int n) {
//	struct Node* newNode;
//	struct Node* last;
//
//	struct Node* first = (struct Node*)malloc(sizeof(struct Node));
//	if (first) {
//		first->data = A[0];
//		first->next = NULL;
//		last = first;
//
//		for (int i = 1; i < n; i++) {
//			newNode = (struct Node*)malloc(sizeof(struct Node));
//			if (newNode) {
//				newNode->data = A[i];
//				newNode->next = NULL;
//				if (last) {
//					last->next = newNode;
//					last = newNode;
//				}
//			}
//		}
//	}
//
//	return first;
//}
//
//void Display(struct Node* node) {
//	while (node != NULL) {
//		printf("%d ", node->data);
//		node = node->next;
//	}
//	printf("\n");
//}
//
//int CountNodes(struct Node* node) {
//	int count = 0;
//
//	while (node != NULL) {
//		count++;
//		node = node->next;
//	}
//
//	return count;
//}
//
//// Only works on a sorted list
//void RemoveDuplicates(struct Node* node) {
//	if (node) {
//		struct Node* q = node->next;
//
//		while (q != NULL) {
//			if (node->data == q->data) {
//				node->next = q->next;
// 				free(q);
//				q = node->next;
//			}
//			else {
//				node = q;
//				q = q->next;
//			}
//		}
//	}
//}
//
//int main() {
//	struct Node* head = NULL;
//
//	int A[] = { 1, 1 ,1, 1, 1 };
//	head = Create(A, 5);
//
//	Display(head);
//
//	RemoveDuplicates(head);
//
//	Display(head);
//
//	return 0;
//}
