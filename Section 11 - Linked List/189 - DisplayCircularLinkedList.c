//#include <stdio.h>
//#include <stdlib.h>
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//// Create a circular linked list
//struct Node* create(int A[], int n) {
//	int i;
//	struct Node* head = (struct Node*)malloc(sizeof(struct Node));
//
//	if (head) {
//		head->data = A[0];
//		head->next = head;
//		struct Node* last = head;
//		if (last) {
//			for (i = 1; i < n; i++) {
//				struct Node* node = (struct Node*)malloc(sizeof(struct Node));
//				if (node) {
//					node->data = A[i];
//					node->next = last->next;
//					last->next = node;
//					last = node;
//				}
//			}
//		}
//	}
//	return head;
//}
//
//void Display(struct Node* head) {
//	struct Node* p = head;
//	do {
//		printf("%d ", p->data);
//		p = p->next;
//	} while (p != head);
//	printf("\n");
//}
//
//void DisplayR(struct Node* head, struct Node* node) {
//	static int flag = 0;
//
//	// This if statement is executed on the first time going through
//	// this function. It is executed on each subsequent function call
//	// where node is not equal to head. When it is equal (linked list
//	// loop has been cycled through), because flag is already set to
//	// 1 it will not execute, and therefore it simply completes the
//	// function and returns. Nifty!
//	if (node != head || flag == 0) {
//		flag = 1;
//		printf("%d ", node->data);
//		DisplayR(head, node->next);
//	}
//
//	flag = 0;
//}
//
//int main() {
//	int A[] = { 2, 3, 4, 5, 6 };
//
//	struct Node* head = create(A, 5);
//	DisplayR(head, head);
//
//	return 0;
//}