//#include <stdio.h>
//#include <stdlib.h>
//
//struct Node {
//	int data;
//	struct Node* next;
//};
//
//struct Node* Create(int A[], int n) {
//	struct Node* newNode;
//	struct Node* last;
//
//	struct Node* first = (struct Node*)malloc(sizeof(struct Node));
//	if (first) {
//		first->data = A[0];
//		first->next = NULL;
//		last = first;
//
//		for (int i = 1; i < n; i++) {
//			newNode = (struct Node*)malloc(sizeof(struct Node));
//			if (newNode) {
//				newNode->data = A[i];
//				newNode->next = NULL;
//				if (last) {
//					last->next = newNode;
//					last = newNode;
//				}
//			}
//		}
//	}
//
//	return first;
//}
//
//void Display(struct Node* node) {
//	while (node != NULL) {
//		printf("%d ", node->data);
//		node = node->next;
//	}
//	printf("\n");
//}
//
//int CountNodes(struct Node* node) {
//	int count = 0;
//
//	while (node != NULL) {
//		count++;
//		node = node->next;
//	}
//
//	return count;
//}
//
//// Merge sorted list2 into sorted list1.
//struct Node* Merge(struct Node* p, struct Node* q) {
//	struct Node* last = NULL;
//	struct Node* head = NULL;
//
//	if (p && q) {
//		if (p->data < q->data) {
//			last = p;
//			p = p->next;
//		}
//		else {
//			last = q;
//			q = q->next;
//		}
//		head = last;	// Set new head of list, dependent on which first node has the lowest value.
//
//		while (p && q) {
//			if (p->data < q->data) {
//				last->next = p;
//				last = p;
//				p = p->next;
//			}
//			else {
//				last->next = q;
//				last = q;
//				q = q->next;
//			}
//		}
//
//		if (p) {
//			last->next = p;
//		}
//		else {
//			last->next = q;
//		}
//	}
//
//	return head;
//}
//
//int main() {
//	int A[] = { 1, 3, 5, 7, 9, 11 };
//	struct Node* head2 = Create(A, 6);
//	int B[] = { 0, 2, 4, 6, 8, 10 }; 
//	struct Node* head1 = Create(B, 6);
//
//	Display(head1);
//	Display(head2);
//
//	// Maybe head1 and head2 should be set to NULL after this.
//	struct Node* head3 = Merge(head1, head2);
//
//	Display(head3);
//
//	return 0;
//}
