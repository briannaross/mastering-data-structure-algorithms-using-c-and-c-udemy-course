//#include <stdio.h>
//
//int main() {
//
//	// Method 1 (same in c and c++)
//	int A[3][4] = { {1, 2, 3, 4}, {2, 4, 6, 8}, {3, 5, 7, 9} }; // Allocated on stack
//
//	// Method 2 (c++ style, use malloc for c style)
//	int* B[3];	// int pointer allocated on stack
//	B[0] = new int[4];	// Array allocated on heap and pointer assigned to B[0]
//	B[1] = new int[4];
//	B[2] = new int[4];
//
//	// Method 3 (c++ style, use malloc for c style)
//	int** C;	// Double int pointer allocated on stack
//	C = new int*[3];	// Array of int pointers allocated on heap and pointer assigned to C
//	C[0] = new int[4];	// Array allocated on heap and pointer assigned to C[0]
//	C[1] = new int[4];
//	C[2] = new int[4];
//
//	// Copy array A into B and C
//	for (int i = 0; i < 3; i++) {
//		for (int j = 0; j < 4; j++) {
//			C[i][j] = B[i][j] = A[i][j];
//		}
//	}
//
//	// Print arrays A, B and C. They should all contain the same values
//	for (int i = 0; i < 3; i++) {
//		for (int j = 0; j < 4; j++) {
//			printf("A: %d, B: %d, C: %d\n",A[i][j], B[i][j], B[i][j]);
//		}
//		printf("\n");
//	}
//
//
//	// c style
//	//
//	// int* B[3];
//	// B[0] = (int *)malloc(4 * sizeof(int));
//	// B[1] = (int *)malloc(4 * sizeof(int));
//	// B[2] = (int *)malloc(4 * sizeof(int));
//	//
//	// int** C;
//	// C = (int **)malloc(3 * sizeof(int *));
//	// C[0] = (int *)malloc(4 * sizeof(int));
//	// C[1] = (int *)malloc(4 * sizeof(int));
//	// C[2] = (int *)malloc(4 * sizeof(int));
//
//
//	return 0;
//}
