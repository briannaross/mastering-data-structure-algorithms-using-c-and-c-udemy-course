//#include <stdio.h>
//#include <stdlib.h>
//
//int main() {
//
//	// C style
//	int *a;
//	a = (int*)malloc(5 * sizeof(int)); // Allocate contiguous memory on the heap for 5 ints, then cast an int ptr and assign to a
//	a[0] = 3;
//	a[1] = 5;
//	a[2] = 7;
//	a[3] = 9;
//	a[4] = 11;
//
//	int* b;
//	b = (int*)malloc(10 * sizeof(int));  // Allocate contiguous memory on the heap for 10 ints, then cast an int ptr and assign to b
//
//	for (int i = 0; i < 5; i++) {	// Copy elements in a to b
//		b[i] = a[i];
//	}
//
//	for (int i = 0; i < 5; i++) {	// Print the arrays and confirm assignment
//		printf("%d %d\n", a[i], b[i]);
//	}
//	
//	free(a);	// Release the memory occupied by the array pointed to by p
//	a = b;		// Point a to where b is pointing
//	b = NULL;	// Set b to point to nothing
//
//
//
//
//
//	// C++ style
//	int* p = new int[5];	// Allocate contiguous memory on the heap for 5 ints and assign to p
//	p[0] = 5;
//	p[1] = 8;
//	p[2] = 9;
//	p[3] = 6;
//	p[4] = 4;
//
//	int* q = new int[10];	// Allocate contiguous memory on the heap for 10 ints and assign to q
//
//	for (int i = 0; i < 5; i++) {	// Copy elements in p to q
//		q[i] = p[i];
//	}
//
//	for (int i = 0; i < 5; i++) {	// Print the arrays and confirm assignment
//		printf("%d %d\n", p[i], q[i]);
//	}
//
//	delete[] p;	// Delete old array
//	p = q;		// Point p to where q is pointing
//	q = NULL;	// Set q to point to nothing
//
//
//}
