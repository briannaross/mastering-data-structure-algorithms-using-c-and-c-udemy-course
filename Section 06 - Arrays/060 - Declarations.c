//#include <stdio.h>
//
//int main() {
//
//	int A[5];
//	int B[5] = { 1, 2, 3, 4, 5 };
//	int C[10] = { 2, 4, 6 };
//	int D[5] = { 0 };
//	int E[] = { 1, 2, 3, 4, 5 };
//
//	// Add breakpoint here and inspect locals to see how the arrays have been initialised.
//
//	// Print addresses of array A elements
//	// Note that ints in VS2019 on Win10 64-bit are 4 bytes (32 bits)
//	for (int i = 0; i < 5; i++) {
//		printf("%u ", &A[i]);
//	}
//
//	return 0;
//}
