//#include <stdio.h>
//#include <stdlib.h>
//
//int main() {
//
//	// Stack allocation
//	int A[5] = { 2, 4, 6, 8, 10 };
//
//	// Heap allocation
//	int* p;
//	p = (int *)malloc(5 * sizeof(int));
//	p[0] = 3;
//	p[1] = 5;
//	p[2] = 7;
//	p[3] = 9;
//	p[4] = 11;
//
//	// Add breakpoint here and inspect locals to see how the arrays have been initialised.
//	// Note how p displays the first element of the array, as that's what the p pointer points to.
//
//	for (int i = 0; i < 5; i++) {
//		printf("%d ", A[i]);
//	}
//	printf("\n");
//	for (int i = 0; i < 5; i++) {
//		printf("%d ", p[i]);
//	}
//
//	return 0;
//}
