//#include <stdio.h>
//
//int main() {
//	char str[] = "Lorem Ipsum Dolor Sit Amet...";
//
//	// The line below causes a crash when trying to change it because compiler seems to want to treat it as a const char*
//	// https://stackoverflow.com/questions/36527482/access-violation-writing-location-error-during-string-assignment
//	// char *str = "LOREM IPSUM DOLOR SIT AMET...";
//
//	int i;
//
//	for (i = 0; str[i] != NULL; i++) {
//		if (str[i] >= 65 && str[i] <= 90) {
//			str[i] += 32;
//		}
//		else if (str[i] >= 97 && str[i] <= 122) {
//			str[i] -= 32;
//		}
//	}
//
//	printf("String: '%s'\n", str);
//
//	return 0;
//}