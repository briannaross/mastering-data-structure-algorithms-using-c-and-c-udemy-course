//#include <stdio.h>
//#include <stdlib.h>
//
//// Note: No consideration given for whether upper casematches a lower case.
//int CompareStrings(char* str1, char* str2) {
//	int length;
//	for (length = 0; str1[length] != '\0'; length++); // Get length
//
//	for (int i = 0; str1[i] != '\0' && str2[i] != '\0'; i++) {
//		char c1 = str1[i];
//		char c2 = str2[i];
//		if (c1 >= 97 && c1 <= 122) {
//			c1 -= 32;
//		}
//		if (c2 >= 97 && c2 <= 122) {
//			c2 -= 32;
//		}
//		if (c1 != c2) {
//			return str1[i] - str2[i];
//		}
//	}
//
//	return 0;
//}
//
//// Compare one string to a second that is the first string reversed
//int IsPalindromeCopy(char* str1) {
//	int length;
//	for (length = 0; str1[length] != '\0'; length++); // Get length
//	
//	char *str2;
//	str2 = (char*)malloc(length);
//	
//	int j = length - 1;
//	for (int i = 0; str1[i] != '\0'; i++) {
//		str2[j] = str1[i];
//		j--;
//	}
//	str2[length] = '\0';
//
//	for (int i = 0; str1[i] != '\0'; i++) {
//		char c1 = str1[i];
//		char c2 = str2[i];
//		if (c1 >= 97 && c1 <= 122) {
//			c1 -= 32;
//		}
//		if (c2 >= 97 && c2 <= 122) {
//			c2 -= 32;
//		}
//		if (c1 != c2) {
//			return 0;
//		}
//	}
//	return 1;
//}
//
//// Can also reverse the string and compare two strings (use reverse and compare functions).
//int IsPalindromeSwap(char* str1) {
//	int upper;
//	for (upper = 0; str1[upper] != '\0'; upper++); // Get length
//	upper--;
//
//	for (int lower = 0; lower < upper; lower++, upper--) {
//		char c1 = str1[lower];
//		char c2 = str1[upper];
//		if (c1 >= 97 && c1 <= 122) {
//			c1 -= 32;
//		}
//		if (c2 >= 97 && c2 <= 122) {
//			c2 -= 32;
//		}
//		if (c1 != c2) {
//			return 0;
//		}
//	}
//	return 1;
//}
//
//int main() {
//	char str1[] = "PoOpy";
//	char str2[] = "Painting y";
//
//	//int compareVal = CompareStrings(str1, str2);
//	//if (compareVal < 0) {
//	//	printf("Strings do not match.\n'%s' is smaller then '%s'.\n", str1, str2); 
//	//}
//	//else if (compareVal > 0) {
//	//	printf("Strings do not match.\n'%s' is greater then '%s'.\n", str1, str2);
//	//}
//	//else {
//	//	printf("Strings match.\n");
//	//}
//
//	//if (IsPalindromeSwap(str1)) {
//	//	printf("String is a palindrome.\n");
//	//}
//	//else {
//	//	printf("String is not a palindrome.\n");
//	//}
//
//	if (IsPalindromeCopy(str1)) {
//		printf("String is a palindrome.\n");
//	}
//	else {
//		printf("String is not a palindrome.\n");
//	}
//
//	return 0;
//}