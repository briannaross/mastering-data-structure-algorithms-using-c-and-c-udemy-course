//#include <stdio.h>
//
//// Finds duplicate letters in a (lowercase) string by using bitwise arrays.
//int main() {
//
//	char A[] = "finding";	// Initialise a string
//	long int H = 0;			// Initiialise an int to be used as a bitwise array. long ints are 4 bytes/32 bits. 26 bits are required for an alphabet
//	long int x = 0;			// Temp bitwise array
//
//	for (int i = 0; A[i] != '\0'; i++) { // For each element in the string
//		x = 1;				// Init temp array so the rightmost bit is set to 1
//		x = x << A[i] - 97;	// Shift that bit by the amount (A[i] - 97), representing that alphabetic character's place in the alphabet proper
//		if ((x & H) > 0) {	// Perform masking, check for non-zero indicating that a match (duplicate) has been found
//			printf("Duplicate found: %c\n", A[i]);
//		}
//		else {
//			H = x | H;		// Merge the two bitwise arrays. Do this because no match was found, but the character has now been found once and can be checked against in subsequent iterations
//		}
//	}
//
//	return 0;
//
//}
