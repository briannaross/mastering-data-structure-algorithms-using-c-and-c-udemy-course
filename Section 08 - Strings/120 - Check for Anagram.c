//#include <stdio.h>
//
//int main() {
//
//	char A[] = "decimal";
//	char B[] = "medical";
//
//
//	// Hash solution
//	//int i;
//	//int H[26] = { 0 };
//
//	//for (i = 0; A[i] != '\0'; i++) {
//	//	H[A[i] - 97] += 1;
//	//}
//
//	//for (i = 0; B[i] != '\0'; i++) {
//	//	H[B[i] - 97] -= 1;
//	//}
//
//	//for (i = 0; H[i] <= 26; i++) {
//	//	if (i == 26) {
//	//		printf("Anagram found\n");
//	//		break;
//	//	}
//	//	else if (H[i] != 0) {
//	//		printf("Not an anagram\n");
//	//		break;
//	//	}
//	//}
//	//
//
//
//
//	// Bitwise comparison
//	long int hash = 0;			// Initiialise an int to be used as a bitwise array. long ints are 4 bytes/32 bits. 26 bits are required for an alphabet
//	long int x = 0;				// Temp bitwise array
//
//	for (int i = 0; A[i] != '\0'; i++) { // For each element in the string
//		x = 1;					// Init temp array so the rightmost bit is set to 1
//		x = x << (A[i] - 97);	// Shift that bit by the amount (A[i] - 97), representing that alphabetic character's place in the alphabet proper
//		hash = x ^ hash;		// XOR the two bitwise arrays. Do this to negate any match. If both words are anagrams then there will be two times any given number of occurences in one word,
//								// so if we keep XOR-ing the hash then if the two words are anagrams the hash should be all zeroes.
//	}
//
//	for (int i = 0; B[i] != '\0'; i++) {
//		x = 1;
//		x = x << (B[i] - 97);
//		hash = x ^ hash;
//	}
//
//	if (hash == 0) {
//		printf("Anagram found\n");
//	}
//	else {
//		printf("Not an anagram\n");
//	}
//
//	return 0;
//}