//#include <iostream>
//
//using namespace std;
//
//class Hash {
//private:
//	int offset = 0;
//	int length = 0;
//	int* A;
//public:
//	Hash(int, int);
//	~Hash();
//	void Increment(int);
//	int GetValue(int);
//	int GetLength();
//	int GetOffset();
//	void PrintHash();
//};
//
//Hash::Hash(int o, int l) {
//	offset = o;
//	length = l;
//	A = new int[length];
//	fill_n(A, length, 0);
//}
//
//Hash::~Hash() {
//	delete[] A;
//}
//
//void Hash::Increment(int i) {
//	int index = i - offset;
//	if (index >= 0 && index < length) {
//		A[index]++;
//	}
//}
//
//int Hash::GetValue(int i) {
//	int index = i - offset;
//	if (index >= 0 && index < length) {
//		return A[index];
//	}
//	return -1;
//}
//
//int Hash::GetOffset() {
//	return offset;
//}
//
//int Hash::GetLength() {
//	return length;
//}
//
//void FindDuplicates(char* str, Hash& hash) {
//	int length;
//	for (length = 0; str[length] != '\0'; length++);
//	length--;
//	
//	for (int i = 0; i < length; i++) {
//		if ((str[i] >= 'A' && str[i] <= 'Z') || (str[i] >= 'a' && str[i] <= 'z')) {
//			hash.Increment(str[i]);
//		}
//	}
//
//	for (int i = 0; i < hash.GetLength(); i++) {
//		if (hash.GetValue(i + hash.GetOffset()) > 1) {
//			cout << (char)(i + hash.GetOffset()) << ", ";
//		}
//	}
//}
//
//void Hash::PrintHash() {
//	for (int i = 0; i < length; i++) {
//		cout << A[i] << " ";
//	}
//	cout << endl;
//}
//
//int main() {
//	char str[] = "Lorem ipsum dolor amet...";
//	Hash letterHash('A', 'z'-'A' + 1);
//
//	FindDuplicates(str, letterHash);
//
//	return 0;
//}