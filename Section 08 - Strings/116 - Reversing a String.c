//#include <stdio.h>
//#include <stdlib.h>
//
//void Swap(char* a, char* b) {
//	char temp = *a;
//	*a = *b;
//	*b = temp;
//}
//
//void CopyReverse(char* str1) {
//	int length;
//	for (length = 0; str1[length] != '\0'; length++); // Get length
//
//	// Dynamically allocate a string char array.
//	// Note how this is different to allocating an int array.
//	char *str2;
//	str2 = (char*)malloc(length);
//
//	int j = length - 1;
//	for (int i = 0; str1[i] != '\0'; i++) {
//		str2[j] = str1[i];
//		j--;
//	}
//	str2[length] = '\0';
//
//	for (int i = 0; str2[i] != '\0'; i++) {
//		str1[i] = str2[i];
//	}
//}
//
//void SwapReverse(char* str) {
//	int upper;
//
//	for (upper = 0; str[upper] != '\0'; upper++); // Get length
//
//	// Decrement upper because array at that index is a null character
//	upper--;
//
//	for (int lower = 0; upper > lower; lower++, upper--) {
//		Swap(&str[lower], &str[upper]);
//	}
//}
//
//
//int main() {
//	char str1[] = "abcdef"; // Mutable, modern C++ compilers make char str* as const
//	int i;
//
//	//CopyReverse(str1);
//	SwapReverse(str1);
//
//	printf("%s", str1);
//
//	return 0;
//}