#include <stdio.h>

Swap(char* c1, char* c2) {
	char temp = *c1;
	*c1 = *c2;
	*c2 = temp;
}

int Permutations1(char s[], int k) {
	static int count = 0;
	static int A[10] = { 0 };
	static char Res[10];

	if (s[k] == '\0') {
		Res[k] = '\0';
		printf("%s\n", Res);
		count++;
	}
	else {
		for (int i = 0; s[i] != '\0'; i++) {
			if (A[i] == 0) {
				Res[k] = s[i];
				A[i] = 1;
				Permutations1(s, k + 1);
				A[i] = 0;
			}
		}
	}
	return count;
}

int Permutations2(char s[], int l, int h) {
	static int count = 0;
	if (l == h) {
		printf("%s\n", s);
		count++;
	}
	else {
		for (int i = l; i <= h; i++) {
			Swap(&s[l], &s[i]);
			Permutations2(s, l + 1, h);
			Swap(&s[l], &s[i]);
		}
	}
	return count;
}

int main() {
	char str[] = "bag";
	int len;
	for (len = 0; str[len] != '\0'; len++);
	len--;

	printf("1st method\n");
	printf("Number of permutations: %d\n", Permutations1(str, 0));

	printf("\n2nd method\n");
	printf("Number of permutations: %d\n", Permutations2(str, 0, len));

	return 0;

}
