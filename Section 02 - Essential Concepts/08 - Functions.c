//#include <stdio.h>
//
//int add(int a, int b) { // Prototype/Header/Signature
//	// Parameters here are called 'formal parameters'
//	int c;
//
//	c = a + b;
//
//	return c;
//}
//
//int main() {
//	int x, y, z;
//
//	x = 10;
//	y = 5;
//
//	z = add(x, y); // Parameters here are called 'actual parameters'
//
//	printf("Sum is %d", z);
//
//	return 0;
//}
