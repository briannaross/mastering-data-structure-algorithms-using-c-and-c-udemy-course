//#include <iostream>
//using namespace std;
//
//// Call by reference (C++ only)
//// Don't use this for functions requiring complex logic, use carefully.
//void swap_cbr(int &x, int &y) { // Very simple change to pass by value
//	int temp;
//
//	temp = x;
//	x = y;
//	y = temp;
//}
//
//int main() {
//	int a, b;
//
//	a = 10;
//	b = 20;
//
//	swap_cbr(a, b);
//	
//	cout << "Call by reference: " << a << " " << b << endl;
//
//	return 0;
//}
