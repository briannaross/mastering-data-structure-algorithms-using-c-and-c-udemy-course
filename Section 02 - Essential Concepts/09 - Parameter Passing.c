//#include <stdio.h>
//
//// Pass by value
//void swap_pbv(int x, int y) {
//	int temp;
//
//	temp = x;
//	x = y;
//	y = temp;
//}
//
//// Call by address
//void swap_cba(int* x, int* y) {
//	int temp;
//
//	temp = *x; // Accessing the data referenced by the pointers
//	*x = *y;
//	*y = temp;
//}
//
//int main() {
//	int a, b;
//
//	a = 10;
//	b = 20;
//
//	// Won't swap a and b!
//	swap_pbv(a, b);
//	printf("Pass by value: %d %d\n", a, b);
//
//	swap_cba(&a, &b);
//	printf("Call by address: %d %d\n", a, b);
//
//	return 0;
//}
