// Pointer to a structure

//#include <stdio.h>
//
//struct Rectangle {
//	int length;
//	int breadth;
//};
//
//int main() {
//
//	//struct Rectangle r = { 10, 5 }; // Occupies the space of two ints (varies by compiler)
//	//struct Rectangle* p = &r; // Occupies only the size the pointer itself requires.
//
//	//r.length = 15;
//	//(*p).length = 20; // Clunky but correct
//	//p->length = 20; // Easier
//
//	struct Rectangle* p;
//
//	p = (struct Rectangle *)malloc(sizeof(struct Rectangle)); // Note the cast of the return pointer. Malloc return a void pointer by default, so we cast it to what we want.
//
//	p->length = 10;
//	p->breadth = 5;
//
//
//
//	return 0;
//}
