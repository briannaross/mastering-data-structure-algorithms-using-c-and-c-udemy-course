// References (C++ only, not available in C)

// Useful in parameter passing

//#include <iostream>
//using namespace std;
//
//int main() {
//
//	int a = 10;
//	int& r = a; // r is a reference - an alias - to the same memory space as a.
//
//	cout << "Initial value" << endl;
//	cout << "a: " << a << endl;
//
//	r++;
//
//	cout << "Post-increment of r" << endl;
//	cout << "a: " << a << endl;
//	cout << "r: " << r << endl;
//
//	return 0;
//}
