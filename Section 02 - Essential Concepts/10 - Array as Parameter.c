//#include <stdio.h>
//#include <stdlib.h>
//
//// A[] is a pointer to an array.
//// Arrays can't be passed by value, only by address.
//void fun(int A[], int n) {
//// void fun(int *A, int n) { // This works too, but A can be anything, not just an array.
//		int i;
//
//	for (i = 0; i < n; i++) {
//		printf("%d", A[i]);
//	}
//}
//
//
//int *fun2(int n) {
////int [] fun2(int n) { // Should work but VS doesn't like it for some reason. It means to point to an array only.
//	int* p;
//
//	p = (int*)malloc(n * sizeof(int));
//
//	return p;
//}
//
//int main() {
//	int A[5] = { 2, 4, 6, 8, 10 };
//
//	fun(A, 5);
//
//	int* B;
//	B = fun2(5);
//
//
//	return 0;
//}
