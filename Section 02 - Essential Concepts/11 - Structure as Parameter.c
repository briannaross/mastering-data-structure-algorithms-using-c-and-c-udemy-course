//#include <stdio.h>
//
//struct Rectangle {
//	int length;
//	int breadth;
//};
//
//// Call by value
//int area(struct Rectangle r1) {
//	return r1.length * r1.breadth;
//}
//
//// Call by address
//void changeLength(struct Rectangle* p, int l) {
//	p->length = l;
//}
//
//
//struct Test {
//	int A[5];
//	int n;
//};
//
//// Copy of the struct is passed. The entire array in the struct is copied too.
//void fun(struct Test t1) {
//
//	// Array not changed because the struct it's contained in is passed "call by value".
//	t1.A[0] = 10;
//	t1.A[1] = 9;
//}
//
//
//int main() {
//
//	struct Rectangle r = { 10,5 };
//	changeLength(&r, 20);
//	printf("%d", area(r));
//
//
//	// You can pass a structure as a value even if it has an array inside!
//	struct Test t = { {2, 4, 6, 8, 10}, 5 };
//	fun(t);
//
//	return 0;
//}
